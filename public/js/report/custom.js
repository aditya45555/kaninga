$(document).ready(function(){
	$('.delete').click(function(){
		ttl = $(this).attr('ttl');
		link = $(this).attr('link');
		
		$('.modal-title').html('Deletion Confirmation');
		$('.modal-body').html('Are you sure to delete <b>' + ttl + '</b> ?');
		$('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button><a href="'+ link +'" type="button" class="btn btn-primary">Delete</a>');
		$('.btn-delete-modal').trigger('click');
		
	    return false;
	});
	
	$('.retur').click(function(){
		ttl = $(this).attr('ttl');
		link = $(this).attr('link');
		
		$('.modal-title').html('Retur Confirmation');
		$('.modal-body').html('Are you sure to retur transaction <b>' + ttl + '</b> ?');
		$('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button><a href="'+ link +'" type="button" class="btn btn-primary">Retur</a>');
		$('.btn-delete-modal').trigger('click');
		
	    return false;
	});
	
	$('#disabledschedule').click(function(){
		ttl = $(this).attr('ttl');
		link = $(this).attr('link');
		
		$('.modal-title').html('Disabled Schedule Confirmation');
		$('.modal-body').html('Are You Sure Disabled Schedule ?');
		$('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button><a href="'+ link +'" type="button" class="btn btn-primary">Disabled</a>');
		$('.btn-delete-modal').trigger('click');
		
	    return false;
	});
	
	
	$('#tipe').change(function(){
		if($("#tipe").val()=="Client")
		{
			$("#dividclient").css("display","block");	
			$("#dividcontributor").css("display","none");	
		}
		else if($("#tipe").val()=="Contributor")
		{
			$("#dividclient").css("display","none");	
			$("#dividcontributor").css("display","block");	
		}
		else
		{
			$("#dividclient").css("display","none");
			$("#dividcontributor").css("display","none");	
		}
	});
	
	$('#findbydate').change(function(){
		if($("#findbydate").val()=="date")
		{
			$("#finddate").css("display","block");
			$("#findtext").css("display","none");
				
		}
		else
		{
			$("#finddate").css("display","none");
			$("#findtext").css("display","block");
		}
	});
	
	$('#description').change(function(){
		if($("#description").val()=="Videos")
		{
			$("#link").css("display","block");
			$("#image").css("display","none");
				
		}
		else
		{
			$("#link").css("display","none");
			$("#image").css("display","block");
		}
	});
	
	$('#select-tags').select2({
          // specify tags
          tags: ["Movie", "Cinema", "Favorite","Family","Horror","Documentary"]
        });
	
	$('.datepicker').datepicker({ format: 'dd-mm-yyyy' });
	
	$( "#studioselect" ).change(function() {
		var studioid = $("#studioselect").val();
  		window.location.href = "/inventory/index?studioid="+studioid;
	});
	
	$( "#typeaddschedule" ).change(function() {
		
		var typeaddschedule = $("#typeaddschedule").val();
		if(typeaddschedule == "manual")
		{
			$("#panelmanual").fadeIn("300");
			$("#panelweek").fadeOut("300");
		}
		else
		{
			$("#panelweek").fadeIn("300");
			$("#panelmanual").fadeOut("300");
		}
		$("[id=texttypeaddschedule]").val(typeaddschedule);
	});
	
	$( "#optionyear" ).change(function() {
		
		var year = $(this).val();
		if (year!='')
			window.location.href = "/dashboard/dash_contributor?year="+year;
	});
});