$(document).ready(function() {
$( "div#score" ).click(function() {
  var point = $(this).find('input:hidden[name=score]').val();
  var movieid = $(this).attr("movieid");
  $.post("/rating/insertrating",
	 {
	 	point : point,
	 	movieid : movieid
	 },
	function(data,status)
	{	
		if(data=="SUCCESS")
		{
			$(".alert-success").fadeIn(500);
        	$(".alert-success").delay(500).fadeOut(500);
		}
	});     	
});
});