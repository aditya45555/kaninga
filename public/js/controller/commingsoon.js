$(document).ready(function() {	
	
	$(document).on('click', '.subdistrict', function() {
		var act = $('.subdistrict.active').attr('data-nmr');
		$('.subdistrict').removeClass('collapsed');
		$('#collapsedisctrict'+act).removeClass('in');
		$('#collapsedisctrict'+act).addClass('collapse');
		$('#accordion a').removeClass('active');
		$(this).addClass('collapsed');
		$(this).addClass('active');		
		    	
		return false;
	});
	
	$(document).on('click', '.seeschedules', function() {
		$('.detil-schedules').addClass('in');
		$(this).addClass('active');
		return false;
	});
	
	$(document).on('click', '.seeschedules.active', function() {
		$('.detil-schedules').removeClass('in');
		$(this).removeClass('active');  	
		return false;
	});		
	
	function hasClass(element, cls) {
	    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
	}	
});