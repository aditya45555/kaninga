$(document).ready(function() {	
	
	$(document).on('click', '.subdistrict', function() {
		var act = $('.subdistrict.active').attr('data-nmr');
		$('.subdistrict').removeClass('collapsed');
		$('#collapse'+act).removeClass('in');
		$('#collapse'+act).addClass('collapse');
		$('#accordion a').removeClass('active');
		$(this).addClass('collapsed');
		$(this).addClass('active');		
		    	
		return false;
	});			
});
