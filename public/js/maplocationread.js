$(function() {
	//var map = L.map('map').setView([-6.121435, 106.774124], 5);
	var map = L.map('map').setView([-6.121435, 106.774124], 8);
	
	var id=document.getElementById("studio").value;
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYmxhbmQiLCJhIjoiY2lrZXB6azE5MDA2MHYybTJqNGF4OHc3MSJ9.ueb5vkLhETIQrCFHTi0XGQ', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
				'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="http://mapbox.com">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(map);
	$.get( "/studio/getlatlong?id="+id, function( data ) {
		var obj = JSON.parse(data);
		for(var i in obj)
            {	
             	L.marker([obj[i].latitude, obj[i].longitude]).addTo(map)
                .bindPopup('<a href="#"'+obj[i].description+'</a>').openPopup();
            }	
		});
});


