$(document).ready(function(){
	$('#tanggal').change(function(){
		$('#teater').removeAttr("disabled");
	});
	
	$('#btncari').click(function(){
		if ($('#jumlah').val() == ""){
			alert('isi jumlah tiket');
			return;
		};
		var kotas = $('#kota').find('option:selected').attr("cariidkota");
		var teaters = $('#teater').find('option:selected').val();
		var tanggals = $('#tanggal').datepicker({dateformat: 'dd-mm-YY'}).val();
		var waktus = $('#waktu').find('option:selected').val();
		var jumlahs = $('#jumlah').val();
		var idfilms = $('#idfilm').val();
		
		$.post(
			"/buyticket/caridata",
			{kotav : kotas,
			 teaterv : teaters, 
			 tanggalv: tanggals, 
			 waktuv : waktus, 
			 jumlahv : jumlahs, 
			 idfilmv : idfilms
			}
		)
		.done(function(data){
			$(".lihatdata").empty();
			
			
			if (JSON.parse(data) == ""){
				$(".lihatdata").append("<tr><td colspan='7' align='center'>Jadwal tidak tersedia</td></tr>");
			}else {
				$.each(JSON.parse(data),function(idx, obj){
					
					if (obj.ket == "ORDER NOW"){
						var tes = (obj.statusbayar/1000).toFixed(3);
						$(".lihatdata").append("<tr>"
						+"<td height='30'>"+obj.name+"</td>"
						+"<td height='30'>"+obj.tanggal+"</td>"
						+"<td height='30'>"+obj.waktu+"</td>"
						+"<td height='30'>"+obj.harga+"</td>"
						+"<td height='30'>"+obj.jumlah+"</td>"
						+"<td height='30' style='color:#006600;'>Rp. "+tes+"</td>"
						+"<td height='30'><button style='background-color:#e65c00;color:white;'> "+obj.ket+"</button></td>"
						+"</tr>");
						//return;
					}
					else{
						$(".lihatdata").append("<tr>"
						+"<td height='30'>"+obj.name+"</td>"
						+"<td height='30'>"+obj.tanggal+"</td>"
						+"<td height='30'>"+obj.waktu+"</td>"
						+"<td height='30'>"+obj.harga+"</td>"
						+"<td height='30'>"+obj.jumlah+"</td>"
						+"<td height='30' style='color:red;'>"+obj.statusbayar+"</td>"
						+"<td height='30'><button style='background-color:gray;color:white;' disabled> "+obj.ket+"</button></td>"
						+"</tr>");
						//return false;
					} 
				});
			}
		});
		
	});
});
