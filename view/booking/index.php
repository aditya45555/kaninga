<!-- Search bar -->
	<div style="margin-top: 55px;" class="search-wrapper">
	    <div style="padding-top: 1.5%;" class="container container--add">
	        <!--form id='search-form' method='post' action="/trailer/index" class="search">
	            <input type="text" class="search__field" name="search" placeholder="Cari">
	            <select name="optionsearch" id="search-sort" class="search__sort" tabindex="0">
	                <option value="1" selected='selected'>Judul Film</option>
	            </select>
	            <button type='submit' class="btn btn-md btn--danger search__button">Cari Film</button>
	        </form-->
	    </div>
	</div>
	<section class="container">
		<div class="order-container">
            <div class="order">
                <img class="order__images" alt='' src="/public/images/tickets.png">
                <p class="order__title">Book a ticket <br><span class="order__descript">and have fun movie time</span></p>
                <!--div class="order__control">
                    <a href="#" class="order__control-btn active">Purchase</a>
                    <a href="#" class="order__control-btn">Reserve</a>
                </div-->
            </div>
        </div>
        <div class="order-step-area">
            <div class="order-step first--step">1. What &amp; Where &amp; When &amp; Choose a sit</div>
        </div>

        <h2 class="page-heading heading--outcontainer">Choose a movie</h2>
        <div class="choose-film">
        	<div class="swiper-container">
        		<div class="swiper-wrapper">
        			<?php
	        		foreach($movie as $dm) {
	        			$image="";
						if (isset($dm['image']))
						{
							if(strlen(trim($dm['image'])) > 0)
							{
								$path_parts = pathinfo($dm['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".c128x202.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	        		?>
	        		<div class="swiper-slide" data-film="<?php echo $dm['name']; ?>"> 
	                    <div class="film-images">
	                        <img alt='' src="<?php echo $image; ?>">
	                    </div>
	                    <p class="choose-film__title"><?php echo $dm['name']; ?></p>
	              	</div>
	        		<?php	
	        		}
	        		?>
        		</div>        		
        	</div>
        </div>
        <section class="container">
        	<div class="col-sm-12">
        		<div class="choose-indector choose-indector--film">
                    <strong>Choosen: </strong><span class="choosen-area"></span>
                </div>
        	</div>
        </section>
		<div class="overflow-wrapper">
			<div class="col-sm-12">
				<span style="margin-top: 50px" class="DigitamaWallet"
				    dw-id="transaction007"
				    dw-mid="56b06a991295950cd98b456d"
				    dw-token="<?php echo $token;?>"
				    dw-customer="Indra Setiawan"
				    dw-phone="081809502545"
				    dw-email="blandtocare@gmail.com"
				    dw-price="10000"
				    dw-item="Tiket Bioskop Kaninga 1"
				    dw-redirect="http://kaninga.com/booking/notif">
				    <button class="dw-open">Bayar</button>
				</span>
			</div>
		</div>
	</section>	