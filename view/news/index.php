	<!-- Search bar -->
        <div style="margin-top: 55px;" class="search-wrapper">
	    <div style="padding-top: 1.5%;" class="container container--add">
                <form id='search-form' method='post' action="/news/index" class="search">
                    <input type="text" name="search" class="search__field" placeholder="Cari">
                    <select name="optionsearch" id="search-sort" class="search__sort" tabindex="0">
                        <option value="1" selected='selected'>Judul Berita</option>
                        <option value="2">Tag Berita</option>
                    </select>
                    <button type='submit' class="btn btn-md btn--danger search__button">Cari Berita</button>
                </form>
            </div>
        </div>
        
        <!-- Main content -->
        <section class="container">
            <div class="overflow-wrapper">
                <div class="col-sm-12">
                    <h2 class="page-heading">Berita</h2>   
                    <?php
                    foreach($datanews as $dns)
                    {
                    	$image="";
						if (isset($dns['image']))
						{
							if(strlen(trim($dns['image'])) > 0)
							{
								$path_parts = pathinfo($dns['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".f1300x600.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
                    ?>
	                    <!-- News post article-->
	                    <article class="post post--news">
	                        <a href='/newsdetail/index?newsid=<?php echo $dns['_id']?>' class="post__image-link">
	                            <img alt='' src="<?php echo $image?>" style="width: 100%;height: 600px;">
	                        </a>
	
	                        <h1><a href="/newsdetail/index?newsid=<?php echo $dns['_id']?>" class="post__title-link"><?php echo $dns['title']?></a></h1>
	                        <p class="post__date"><?php echo date("H:i:s d/m/Y",$dns['time_created'])?> </p>
	
	                        <div class="wave-devider"></div>
	
	                        <p class="post__text">
	                        	<?php echo substr($dns['description'],0,250)?>
	                        	<a href="/newsdetail/index?newsid=<?php echo $dns['_id']?>">........(Detail Berita)</a>
	                        </p> 
	
	                        <div class="tags">
	                                <ul>
	                                	<?php
	                                	$tagex = explode(",", $dns['tag']);
	       								for($i = 0;$i<count($tagex);$i++)
	       								{                   
	                                	?>
	                                	
		                                    <li class="item-wrap"><a href="#" class="tags__item"><?php echo $tagex[$i]?></a></li>
		                                    
		                               	<?php
										}
	                                	?>
	                                
	                                </ul>
	                        </div>
	
	                        <div class="devider-huge"></div>
	                    </article> 
	                    <!-- end news post article-->
                    <?php
                    }
                    ?>
                    <div class="pagination">
                        <?php echo $pagination?>
                    </div>

                </div>
            </div>
        </section>
        
        <div class="clearfix"></div>