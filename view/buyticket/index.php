<?php
	$db = Db::init();
	$contri = $db->contributors;
?>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#tanggal" ).datepicker();
  });
  </script>
<section class="container">
	<div class="col-xs-8 col-sm-8 col-md-3 body-menu">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<h6 style="font-size: 15px;" class="bold">KANINGA E-TICKET</h6>
			<div class="custom-container mouseWheelButtons" id="accordion">
		        <a style="padding-left: 0;" data-toggle="collapse" data-parent="#accordion" href="#collapse" class="bold"><h6>MOVIES</h6></a>
			       <div style="margin-bottom: 20px" id="collapse" class="panel-collapse collapse"> 
			        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
			        <div class="carousel">
			            <ul class="gradasi">
			            	<?php
			            	foreach($datamovienw as $dtnw) {
			            		echo '<li>';
								echo '<div class="col-xs-12 col-sm-12 col-md-12 boxgradasi" style="margin:15px 0 15px 0;">';
			            		if (isset($dtnw['image'])){
			            			if(strlen(trim($dtnw['image'])) > 0)
										{
											$path_parts = pathinfo($dtnw['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".c150x80.".$ext;
											$image= CDN.'image/'.$url;
											echo '<a style="padding-left:0;"  href="/buyticket/index?idmovi='.$dtnw['_id'].'">';
											echo '<div class="col-xs-6 col-sm-6 col-md-6"><img style="margin-left:0px" width="150" height="80" src="'.$image.'" alt=""/> </div>';
											$dtcontri = $contri->findOne(array('_id' => new MongoId($dtnw['contributor'])));
											echo '<div class="col-xs-6 col-sm-6 col-md-6" style="padding-left:10px; padding-top:10px;">'.$dtnw['name'].'<br><small>'.$dtcontri['name'].' </small></div>';
											echo '</a>';
										}
								echo '</div>';
								echo '</li>';
			            		}
			            	}
			            	?>
			            </ul>
			        </div>
			       <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
			     </div>  
			 </div>       
			 <div class="custom-container mouseWheelButtons" id="accordion">
		        <a style="padding-left: 0;" data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="bold"><h6>COMMING SOON</h6></a>
			       <div style="margin-bottom: 20px" id="collapse1" class="panel-collapse collapse"> 
			        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
			        <div class="carousel">
			            <ul class="gradasi">
			            	<?php
			            	foreach($datamoviecs as $dtcs) {
			            		echo '<li>';
								echo '<div class="col-xs-12 col-sm-12 col-md-12 boxgradasi" style="margin:15px 0 15px 0;">';
			            		if (isset($dtcs['image'])){
			            			if(strlen(trim($dtcs['image'])) > 0)
										{
											$path_parts = pathinfo($dtcs['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".c150x80.".$ext;
											$image= CDN.'image/'.$url;
											echo '<a style="padding-left:0;" href="/buyticket/index?idmovi='.$dtcs['_id'].'">';
											echo '<div class="col-xs-6 col-sm-6 col-md-6"><img style="margin-left:0px" width="150" height="80" src="'.$image.'" alt=""/> </div>';
											$dtcontri = $contri->findOne(array('_id' => new MongoId($dtcs['contributor'])));
											echo '<div class="col-xs-6 col-sm-6 col-md-6" style="padding-left:10px; padding-top:10px;">'.$dtcs['name'].'<br><small>'.$dtcontri['name'].' </small> </div>';
											echo '</a>';
										}
								echo '</div>';
								echo '</li>';
			            		}
			            	}
			            	?>
			            </ul>
			        </div>
			       <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
			     </div>  
			 </div>  
		    </div>
		</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-9 body-menu">
		<div class="bold">
			<h6 style="margin-top: 10px; margin-bottom: 4px; font-size: 20px">PAY ONLINE NOW</h6>
			<h6 style="margin-top: 0; margin-bottom: 15px;">KANINGA CINEMA TICKET</h6>
		</div>
		<?php if(count($data) > 0 ) {?>
		<div class="col-xs-10 col-sm-10 col-md-10 txtcenter">
		   <div style="text-align: center">
		   			<input type="hidden" id="idfilm" name="idfilm" value="<?php echo $data['_id'];?>" />
					<ul class="nav nav-tabs" role"tablist" style="float:left;">
						 <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($data['name']) ? $data['name'] : '&nbsp;'; ?></a></li>
					</ul>
					<div class="tab-content" style="width: 100%">
				    	<div role="tabpanel" class="tab-pane active" id="home">
				    	  <!--<form method="post" action="/buyticket/caridata">-->	
				    		<table style="width: 100%">
				    			<tr colspan = "6">
				    				<td>Pilih Kota dan Tanggal</td>
				    			</tr>
				    			<tr><td>&nbsp;</td></tr>
				    			<tr>
				    				<td colspan="2">KOTA</td>
				    				<td colspan="2">TANGGAL</td>
				    				<td colspan="2">JUMLAH PENONTON</td>
				    			</tr>
				    			<tr>
				    				<td colspan="2">
				    					<select id="kota" name="kota">
				    						<option value="1" selected>Pilih kota</option>
				    						<?php
				    							foreach ($datakota as $dtk) {
				    								//$cls = ($dtk['_id'] == $idkota) ? 'selected' : '';
				    								echo '<option value="/buyticket/index?idkota='.$dtk['id'].'" cariidkota="'.$dtk['id'].'">'.$dtk['name'].'</option>';	
												}
				    						?>
				    					</select>
				    					<p id="demo"></p>
				    				</td>
				    				<td colspan="2">
				    					<input type="text" id="tanggal" name ="tanggal"/>
				    				</td>
				    				<td colspan="2">
				    					<input type="text" id="jumlah" name="jumlah" placeholder="1" onkeypress="return isNumberKey(event)" required="true"/>
				    				</td>
				    			</tr>
				    			<tr><td>&nbsp;</td></tr>
				    			<tr>
				    				<td colspan="2">THEATER</td>
				    				<td colspan="2">WAKTU</td>
				    			</tr>
				    			
				    			<tr>
				    				<td colspan="2">
				    					<select id="teater" name="teater" class="teater" disabled="true">
				    						<option value="">Cari Studio...</option>
				    					</select>
				    				</td>
				    				<td colspan="2">
				    					<select id="waktu" name="waktu">
				    						<option value="">Waktu tidak tersedia</option>
				    					</select>
				    				</td>
				    				<td colspan="2" align="center">
				    					<input type="button" class="btn btn-default" id="btncari" value="CARI TIKET"/>
				    				</td>
				    			</tr>
				    		</table>
				    	  <!--</form>-->
				    	</div>
		   			</div>
		   			<br>
		   			<div class="col-xs-12 col-sm-12 col-md-12">
		   				<table style="width: 100%">
		   					<thead>
			   					<tr style="outline: thin solid; height: 50px;" align="center">
			   						<td>THEATER</td>
			   						<td>DATE</td>
			   						<td>TIME</td>
			   						<td>PRICE</td>
			   						<td>QTY</td>
			   						<td colspan="2"></td>
			   					</tr>
		   					<tr><td>&nbsp;</td></tr>
		   					</thead>
		   					<tbody class="lihatdata">
		   					</tbody>	
		   				</table>
		   			</div>
		   </div>
		</div>
		<?php } else { ?>
			<h2>No Data Available</h2>
		<?php } ?>
	</div>
	
</section>
<script type="text/javascript">
	$(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
    
    document.getElementById('#kota').onchange = function() {
    window.location.href = this.children[this.selectedIndex].getAttribute('href');
	}
	
</script>