<?php
$db = Db::init();
$schedule = $db->schedules;
$g = $db->genres;
?>
<style>
	.shades {
		color: #826c64;
	}	
	.shades:hover {
		color: #826c64 !important;
	}
</style>
<section class="container">
	<div class="col-xs-12 col-sm-12 col-md-4 body-menu">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 style="font-size: 15px;" class="bold">COMMING SOON</h6>
			<div class="custom-container mouseWheelButtons">
		        <div class="carousel">
		            <ul>
		            	<?php
		            	foreach($datamoviecs as $dtm) {
		            		$cls = ($dtm['_id'] == trim($idmovi)) ? 'active' : '' ;
		            		echo '<li data-value="'.$dtm['_id'].'"><a class="kota '.$cls.'" href="/commingsoon1/index?idmovi='.$dtm['_id'].'">'.$dtm['name'].'</a></li>';
		            	}
		            	?>
		            </ul>
		        </div>
		        <div class="clear"></div>
		    </div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 style="font-size: 15px;" class="bold">THEATER</h6>
			<div class="custom-container mouseWheelButtons2">
		        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
		        <div id="menu" class="carousel">
		            <ul>		            	
		            	<?php
		            		$db = Db::init();
							$n = 0;
							$coltown = $db->place_towns;
							$datastd = $db->studios;
							$dataschedule = $db->schedules;
							$dtmovie = $db->movies;
							$datamoviecs = $dtmovie->find();
						
							foreach($datakota as $dtk){
								echo '<li id="accordion">';
								$clsk = $idkota == $dtk['id'] ? 'active' : 'collapsed';
		            			echo '<a style="padding-left: 0;" data-nmr="'.$n.'" data-toggle="collapse" data-parent="#accordion" href="#collapsedisctrict'.$n.'" id="subdistrict" class="subdistrict '.$clsk.' bold">'.$dtk['name'].'</a>';
								echo '</li>';
								$cls = ($dtk['id'] == $idkota) ? 'in' : 'collapse';
								echo '<div style="margin-bottom: 20px" id="collapsedisctrict'.$n.'" class="panel-collapse '.$cls.'">';
								$dtstd = $datastd->find(array('towns' => $dtk['id']));
								$nn = 0;
								foreach ($dtstd as $valstd) {
									$clst = ($valstd['_id'] == $idstudio) ? 'active':'';
									echo '<li data-value="'.$valstd['_id'].'"><a data-nmr="'.$nn.'" class="studios '.$clst.'" style="padding-left: 0; font-size: 11px;" href="/commingsoon1/index?idmovi='.$idmovi.'&kota='.$dtk['id'].'&studio='.$valstd['_id'].'">'.strtoupper($valstd['name']).'</a></li>';
									
									$clstd = ($valstd['_id'] == $idstudio) ? 'in' : 'collapse';
									echo '<div style="margin-bottom: 20px" id="collapsec'.$nn.'" class="panel-collapse '.$clstd.'">';
									$datasch = $dataschedule->findone(array('movie' => $idmovi, 'studio' => trim($idstudio), 'scheduledate' => array('$gt' => time())));	
									foreach ($datamoviecs as $key) {
										$ada = false;
										if(isset($key['commingsoonstudio'])) {
											foreach($key['commingsoonstudio'] as $cstd) {
												if(($cstd['studioid'] == $idstudio) && ($idmovi == trim($key['_id']))) {
													$ada = true;
													break;
												}
											}
										}
										
										if($ada) 
											echo '<li data-value="'.$key['_id'].'"><a class="active" style="padding-left: 3px;" href="/commingsoon1/index?idmovi='.$idmovi.'&kota='.$dtk['id'].'&studio='.$valstd['_id'].'&movic='.$key['_id'].'">'.strtoupper($key['name']).'</a></li>';
										else										
											echo '<li data-value="'.$key['_id'].'" style="padding-left: 3px; font-size:10px"><a class="shades" style="font-size:10px; padding-left:10% !important;">'.strtoupper($key['name']).'</a></li>';
									}
									echo '</div>';
									$nn++;
								}
								echo '</div>';
							$n++;
							}
							
						?>	            	
		            </ul>		            
		        </div>
		        <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
		        <div class="clear"></div>
		    </div>
		</div>
	</div>
	<!-- untuk kontain film -->
	<div id="detail-commingsoon" class="col-xs-12 col-sm-12 col-md-8 body-detail">
		<?php if(strlen(trim($idmovi)) > 0) { 
			$datam = $dtmovie->findone(array('_id' => new MongoId($idmovi)));
		?>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="col-xs-9 col-sm-9 col-md-9">
				<h6 class="bold"><?php echo isset($datam['name']) ? $datam['name'] : '&nbsp;'; ?></h6>
				<ul class="summary">
					<li>
						<span class="key">genre</span>
						<?php
						$genre = '&nbsp;';
						if(isset($datam['genre'])) {
							$gg = $g->findone(array('_id' => new MongoId($datam['genre'])));
							$genre = $gg['name'];
						}
						?>
						<span class="val"><?php echo $genre; ?></span>
					</li>
					<li>
						<span class="key">produser</span>
						<span class="val"><?php echo isset($datam['produser']) ? $datam['produser'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">sutradara</span>
						<span class="val"><?php echo isset($datam['sutradara']) ? $datam['sutradara'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">penulis</span>
						<span class="val"><?php echo isset($datam['penulis']) ? $datam['penulis'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">produksi</span>
						<span class="val"><?php echo isset($datam['produksi']) ? $datam['produksi'] : '&nbsp;'; ?></span>
					</li>
				</ul>
				<h6 class="bold">CAST</h6>
				<ul class="cast">
					<?php
						if(isset($datam['cast'])) {
							if(strlen(trim($datam['cast'])) > 0) {
								if(strpos($datam['cast'], ',') !== false) {
									$cs = explode(',', $datam['cast']);
									foreach($cs as $ds) {
										if(strlen(trim($ds)) > 0)
										echo '<li><span class="desc">'.$ds.'</span></li>';
									}
								}
								else
									echo '<li><span class="desc">'.$datam['cast'].'</span></li>';
							}
						} 
					?>
				</ul>				
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 duration">
				<h5 class="bold"><?php echo isset($datam['duration']) ? helper::timetosecond($datam['duration']) : '0'; ?></h5>
				<p style="margin-top: -20px; text-align: center;" class="synopsis"><?php echo strlen(trim($datam['duration']) > 0) ? 'Minutes' : '&nbsp'; ?></p>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12 col-sm-12 col-md-12 margin10">
				<h6 class="bold">SYNOPSIS</h6>
				<p class="synopsis"><?php echo isset($datam['synopsis']) ? helper::limitString($datam['synopsis']) : '&nbsp'; ?></p>
				
				<style>
					.schedules:hover {
						color: #ff7d23;
					}
					.detil-schedules {
						padding-top: 5px;
						padding-bottom: 3px;
						border-bottom: 1px solid rgb(130, 108, 100);
						margin-bottom: 5px;
						border-top: 1px solid rgb(130, 108, 100);
					}
				</style>
				
				<!--<h6 class="bold seeschedules active" style="cursor: pointer">JADWAL</h6>-->
				<div class="detil-schedules collapse ">
					<?php
						/*
						$twn = $coltown->findone(array('_id' => new MongoId($idkota)));
												$search = array('Kota', 'Kabupaten');
												$tn = str_replace($search, '', $twn['name']);
												$std = $datastd->findone(array('_id' => new MongoId($idstudio)));*/
						
					?>
					<p style="margin-bottom: 5px;" class="synopsis"><?php //echo ucwords($std['name']).' - '.ucwords($tn); ?></p>
					<ul class="summary">
						<?php												
						/*
						$detschedule = $dataschedule->find(array('studio' => $idstudio, 'movie' => $idmovi, 'active' => '1'));
												foreach($detschedule as $det) {*/
						
						?>
						<li>
							<span class="key"><?php //echo date('d-m-Y', $det['scheduledate']).' '.$det['scheduletime'].' WIB'; ?></span>
						</li>
						<?php// }?>
					</ul>
				</div>
				<ul class="body-detail-menu">
					<!--li><a class="bold" href="/trailer/index?id=<?php //echo $data['_id']; ?>">trailer</a></li>
					<li><a class="bold" href="/booking/index?id=<?php //echo $data['_id']; ?>">buy ticket</a></li-->
				</ul>
			</div>			
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<?php
			if (isset($datam['image']))
			{
				if(strlen(trim($datam['image'])) > 0)
				{
					$path_parts = pathinfo($datam['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".350x500.".$ext;
					$image= CDN.'image/'.$url;
					echo '<img style="margin-left:10px" width="350" height="500" src="'.$image.'" alt=""/>';
				}
			}
			?>
		</div>
		<?php } else { ?>
			<h2>No Data Available</h2>
		<?php } ?>
	</div>
	
</section>
<div class="clearfix"></div>
<script type="text/javascript">
    /*$(document).ready(function() {
        init_MovieList();
        $('.sbHolder').css({'right': '100px'});
        var h = $('.wrapper').height()-($('.header-wrapper').height()+$('.footer-wrapper').height()+$('.search-wrapper').height())-60;
        $('.container').css({'min-height':h});                
    });*/
    $(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 10,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 10,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
</script>