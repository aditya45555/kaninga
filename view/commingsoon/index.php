 <?php
 $db=Db::init();
 $scheduledb = $db->schedules;
 $genredb = $db->genres;
 $contributordb = $db->contributors;
 $votemoviedb = $db->rating_movies;
 $studiodb = $db->studios;
 $galeridb = $db->galerimovies;
 $commentdb = $db->commentmovies;
 $moviedb = $db->movies;
 $towndb = $db->place_towns;
 
 $help = new helper();
 $helpdata = $help->getRatingDisplay(); 
 ?>
 

  	<div style="margin-top: 55px;" class="search-wrapper" >
            <div style="padding-top: 1.5%;" class="container container--add">
                <form id='search-form' method='post' action="/commingsoon/index"  class="search">
                    <input type="text" name="search" class="search__field" placeholder="Cari">
                    <select  name="optionsearch"id="search-sort" class="search__sort" tabindex="0">
                        <option value="1" selected='selected'>Judul Film</option>
                    </select>
                    <button type='submit'  class="btn btn-md btn--danger search__button">Cari Film</button>
                </form>
            </div>
        </div>
        <!-- Main content -->
        <section class="container" >
            <div class="col-sm-12" >
                <h2 class="page-heading" >Coming Soon</h2> 
                <div class="alert alert-success">
  					Rating Movie Diterima
				</div>               
                <?php
                foreach($datamovie as $dm)
                {
                	$datascheduleone =  $scheduledb->findOne(array("movie" => trim($dm['_id'])));
					
					if(!isset($datascheduleone))
					{
	                	$genre=$genredb->findOne(array("_id" => new MongoId(trim($dm['genre']))));
						$contributor=$contributordb->findOne(array("_id" => new MongoId(trim($dm['contributor']))));
						$votemovie = $votemoviedb->find(array("movie" =>trim($dm['_id'])));
						$galerifoto = $galeridb->count(array("movie" => trim($dm['_id']),"description" => "Images" ));
						$galerivideo = $galeridb->count(array("movie" => trim($dm['_id']),"description" => "Videos" ));
						$comment = $commentdb->find(array("movie" => trim($dm['_id'])));
						
	                	$image="";
						if (isset($dm['image']))
						{
							if(strlen(trim($dm['image'])) > 0)
							{
								$path_parts = pathinfo($dm['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".c380x600.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	                ?>
	                <!-- Movie preview item -->
	                	<div class="movie movie--preview release">
	                     <div class="col-sm-3 col-md-2 col-lg-2">
	                            <div class="movie__images">
	                                <img alt='' src="<?php echo $image?>">
	                            </div>
	                            <div class="movie__feature">
	                                <a href="#" class="movie__feature-item movie__feature--comment"><?php echo $comment->count()?></a>
	                                <a href="#" class="movie__feature-item movie__feature--video"><?php echo $galerivideo?></a>
	                                <a href="#" class="movie__feature-item movie__feature--photo"><?php echo $galerifoto?></a>
	                            </div>
	                    </div>
	                    <div class="col-sm-7 col-md-9">
	                            <a href='/moviedetail/index?movieid=<?php echo $dm['_id']?>' class="movie__title link--huge"><?php echo $dm['name']?></a>
	                            <p class="movie__time"><?php echo $dm['duration']?> Jam</p>
	                           
	                            <p class="movie__option"><strong>Genre: </strong><a href="#"><?php echo $genre['name']?></a></p>
	                            <p class="movie__option"><strong>Tanggal Rilis: </strong><?php echo date("d/M/Y",$dm['receiptdate'])?></p>
	                            <p class="movie__option"><strong>Contributor: </strong><a href="#"><?php echo $contributor['name']?></a></p>
	                         
	                           <!-- <div class="movie__btns">
	                                <a href="#" class="btn btn-md btn--warning">book a ticket <span class="hidden-sm">for this movie</span></a>
	                                <a href="#" class="watchlist">Add to watchlist</a>
	                           </div>!-->
	
	                           <div class="preview-footer">
	                                <!--div class="movie__rate"><div class="score" id="score" movieid=<?php //echo trim($dm['_id'])?>  style="display: <?php //echo $helpdata['displayrating']?>"></div><div style="margin-top:35px "></div><span  class="movie__rate-number" style="display:<?php //echo $helpdata['displayrating']?>"><?php //echo $votemovie->count();?> votes</span> <span class="movie__rating" style="left: <?php //echo $helpdata['leftrating']?>"><?php //echo helper::getRatingMovie($dm['_id'])?></span></div-->
	                            </div>
	                    </div>
	
	                    <div class="clearfix"></div>
	                </div>
	                <!-- end movie preview item -->
                <?php
                }
                }
                ?> 
                <div class="coloum-wrapper">
                    <div class="pagination paginatioon--full">
                            <?php echo $pagination;?>
                    </div>
                </div>
            </div>
        </section>
        
        <div class="clearfix"></div>
		<script type="text/javascript">
            $(document).ready(function() {
                init_MovieList();
                $('.sbHolder').css({'right': '100px'});
                var h = $('.wrapper').height()-($('.header-wrapper').height()+$('.footer-wrapper').height()+$('.search-wrapper').height())-60;
                $('.container').css({'min-height':h});                
            });
		</script>
