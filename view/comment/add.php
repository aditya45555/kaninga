<div id="view-comment"></div>
<div class="comment">
    <div class="comment__images">
        <img alt='' src="http://placehold.it/50x50">
    </div>

    <a href='#' class="comment__author"><!--span class="social-used fa fa-facebook"></span--><?php echo ucwords(helper::getUserName($userid)); ?></a>
    <p class="comment__date"><?php echo helper::getTime($time_created); ?></p>
    <p class="comment__message"><?php echo $comment; ?></p>
    <!--a href='#' class="comment__reply">Reply</a-->
</div>