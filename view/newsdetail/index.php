       <!-- Main content -->
        <section style="margin-top: 5%" class="container">
            <div class="col-sm-12">
                <h2 class="page-heading">Detail Berita</h2>

                <div class="post">
                    <div class="post__preview">
                        <div class="swiper-container">
                          <div class="swiper-wrapper">
                              <!--First Slide-->
                              <div class="swiper-slide" data-text='Mauris gravida ipsum vitae libero eget dignissim ipsum egestas.'> 
                                 	<?php
                                 	$image="";
									if (isset($datanews['image']))
									{
										if(strlen(trim($datanews['image'])) > 0)
										{
											$path_parts = pathinfo($datanews['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".f1300x600.".$ext;
											$image= CDN.'image/'.$url;
										}
									}
                                 	?>
                                 <img alt='' src="<?php echo $image?>" style="width: 100%;height: 600px">
                              </div>
                         
                          </div><!-- end swiper wrapper-->
                        </div><!-- end swiper container -->

                 
                    
                    </div>

                    <h1><?php echo $datanews['title']?></h1>
                    <p class="post__date"><?php echo date("H:i:s d/m/Y",$datanews['time_created'])?> </p>
                    <div class="wave-devider"></div>
					<p><?php echo $datanews['description']?></p>
                    
                     <div class="tags">
	                    <ul>
	                    	<?php
	                    	$tagex = explode(",", $datanews['tag']);
							for($i = 0;$i<count($tagex);$i++)
							{                   
	                    	?>
	                    	
	                            <li class="item-wrap"><a href="#" class="tags__item"><?php echo $tagex[$i]?></a></li>
	                            
	                       	<?php
							}
	                    	?>
	                    
	                    </ul>
	                 </div>
                    
                    <div class="info-wrapper">      
                        <div class="share">
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </section>
        
        <div class="clearfix"></div>

   
        
        <script type="text/javascript">
        
            $(document).ready(function() {
                init_SinglePage();
            });
        </script>