		<?php
		$db=Db::init();
		$studiodb = $db->studios;
		$scheduledb = $db->schedules;
		$help = new helper();
 		$helpdata = $help->getRatingDisplay(); 
		?>
		   	
   		<?php
   		foreach($css as $c)
   		{
 		?>
 			<link href="<?php echo $c?>" rel="stylesheet" />
 		<?php  			
   		}
   		?>
   		
   		<?php
   		foreach($font as $c)
   		{
 		?>
 			<link href="<?php echo $c?>" rel="stylesheet" />
 		<?php  			
   		}
   		?>
   		
   		<?php
   		foreach($js as $c)
   		{
 		?>
 			<script src="<?php echo $c?>"></script>
 		<?php  			
   		}
   		?>
   		<input type="hidden" id="movieid" value="<?php echo $movieid?>">        
        <!-- Main content -->
        <section style="margin-top: 5%" class="container">
            <div class="col-sm-12">
                <div class="movie">
                    <h2 class="page-heading"><?php echo $datamovie['name']?></h2>
                    <div class="alert alert-success">
  						Rating Movie Diterima
					</div>
                    <div class="movie__info">
                        <div class="col-sm-4 col-md-3 movie-mobile">
                            <div class="movie__images">
                                <!--span class="movie__rating"><?php //echo helper::getRatingMovie($datamovie['_id'])?></span-->
                                <?php
                                $image="";
								if (isset($datamovie['image']))
								{
									if(strlen(trim($datamovie['image'])) > 0)
									{
										$path_parts = pathinfo($datamovie['image']);
										$f = $path_parts['filename'];
										$ext = $path_parts['extension'];
										$url = $f.".526x723.".$ext;
										$image= CDN.'image/'.$url;
									}
								}
                                
                                ?>
                                <img alt='' src="<?php echo $image?>">
                            </div>
                            <!--div class="movie__rate" style="display: <?php //echo $helpdata['displayrating']?>">Your vote: <div class="score" id="score" movieid=<?php //echo trim($datamovie['_id'])?>></div></div-->
                        </div>

                        <div class="col-sm-8 col-md-9">
                           <p class="movie__time"><?php echo $datamovie['duration']?> Jam</p>
                           
                            <p class="movie__option"><strong>Genre: </strong><a href="#"><?php echo $datagenre['name']?></a></p>
                            <p class="movie__option"><strong>Tanggal Rilis: </strong><?php echo date("d/M/Y",$datamovie['receiptdate'])?></p>
                            <p class="movie__option"><strong>Contributor: </strong><a href="#"><?php echo $datacontributor['name']?></a></p>

                            <a href="#" class="comment-link">Comment:  <?php echo $countcomment?></a>

                            <!--<div class="movie__btns movie__btns--full">
                                <a href="#" class="btn btn-md btn--warning">book a ticket for this movie</a>
                                <a href="#" class="watchlist">Add to watchlist</a>
                            </div>!-->

                            <div class="share">
                                <span class="share__marker">Share: </span>
                                <div class="addthis_toolbox addthis_default_style ">
                                    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                    <a style="width: 100px" class="addthis_button_tweet"></a>&nbsp;
                                    <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <h2 class="page-heading">deskripsi</h2>

                    <p class="movie__describe">
                    <?php
                    echo $datamovie['description'];
                    ?>
                    </p>
                    <h2 class="page-heading">foto &amp; video</h2>
                    
                    <div class="movie__media">
                        <div class="movie__media-switch">
                            <a href="#" class="watchlist list--photo" data-filter='media-photo'><?php echo $countgalerimovieimage;?> foto</a>
                            <a href="#" class="watchlist list--video" data-filter='media-video'><?php echo $countgalerimovievideo;?> video</a>
                        </div>

                        <div class="swiper-container">
                          <div class="swiper-wrapper">
                             
                             <?php
                             foreach($datagalerimovievideo as $gv)
                             {
                             ?>
                             	<!--Slide Video-->
                              <div class="swiper-slide media-video">
                                <a href='<?php echo $gv['link']?>' class="movie__media-item">
                                    <img  alt='' src="http://img.youtube.com/vi/<?php  echo substr($gv['link'], 32);?>/hqdefault.jpg" style="width:400px;height:240px;margin-top: -50px;">
                                </a>
                              </div>
                              	
                             <?php	
                             }
                             ?>
                             
                             <?php
                             foreach($datagalerimovieimage as $gi)
                             {
                             	if(strlen(trim($gi['image'])) > 0)
								{
									$path_parts = pathinfo($gi['image']);
									$f = $path_parts['filename'];
									$ext = $path_parts['extension'];
									$url = $f.".400x240.".$ext;
									$image= CDN.'image/'.$url;
								}
                             ?>
                             
                        	<!--Slide  Image-->
                              <div class="swiper-slide media-photo"> 
                                    <a href='<?php echo $image?>' class="movie__media-item">
                                        <img alt='' src="<?php echo $image?>">
                                     </a>
                              </div>

                             <?php
							 }
                             ?>
                             
                          </div>
                        </div>

                    </div>

                </div>

                <h2 class="page-heading">penayangan &amp; ticket </h2>
                <div class="choose-container">
                   <form class="select select--cinema" method='get' id="testform">
                          <select id="studioid" name="select_item" class="select__sort"  tabindex="0">
                          	<option value="all" selected="selected">Semua</option>
                             <?php
                             foreach($datastudio as $ds)
                             {
                            ?>
                            	<option value="<?php echo $ds['_id']?>"  <?php echo $ds['_id'] == $studioid ? ' selected="selected"' : '';?>"><?php echo $ds['name']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </form>

                    <div class="datepicker">
                      <span class="datepicker__marker"><i class="fa fa-calendar"></i>Tanggal</span>
                      <input type="text" id="datepicker" value='<?php echo date("m/d/Y",$datenow)?>' class="datepicker__input">
                    </div>

                    
                    <div class="clearfix"></div>

                    <div class="time-select">
	                        <?php
                   			foreach($datastudiofilter as $st)
                   			{
                   				$schedule=$scheduledb->find(array("movie" => trim($datamovie['_id']),"active" => "1","scheduledate" => $datenow+1,"studio" => trim($st['_id'])));
                   				$scheduleone=$scheduledb->findOne(array("movie" => trim($datamovie['_id']),"active" => "1","scheduledate" => $datenow+1,"studio" => trim($st['_id'])));
								
								if(strlen($scheduleone['_id'])>0)
								{
								
                   			?>
		                        <div class="time-select__group">
		                            <div class="col-sm-4">
	                   				<?php	
		                   			echo "<p class='time-select__place'>".$st['name']."</p>";				
	                   				?>
		                            </div>
		                            <ul class="col-sm-8 items-wrap">
		                                <?php
		                                foreach($schedule as $sch)
		                                {
		                                ?>
		                                	<li class="time-select__item" data-time='<?php echo $sch['scheduletime']?>'><?php echo $sch['scheduletime']?></li>
		                                <?php	
		                                }
		                                ?>
		                            </ul>
		                        </div>
	                    	<?php
								}
				   			}
	                		?>
	                    </div>
                    
                    <!-- hiden maps with multiple locator-->
                    <div class="mapdet">
                        <div id='mapdet' style="width: 100%; height: 500px"></div> 
                    </div>

                    <h2 class="page-heading cmt">comment (<?php echo $countcomment?>)</h2>

                    <div class="comment-wrapper">
                    	<?php if(isset($_SESSION['userid'])) {?>
                        <form id="comment-form" class="comment-form" method='post'>
                            <textarea class="comment-form__text" placeholder='Add you comment here'></textarea>
                            <label class="comment-form__info">250 characters left</label>
                            <button data-id="<?php echo $datamovie['_id']; ?>" type='button' class="btn btn-md btn--danger comment-form__btn">add comment</button>
                        </form>
                        <?php }?>

                        <div class="comment-sets">
                        	<div id="view-comment"></div>
						<?php
						foreach($datacomment as $dc)
						{
						?>
                        <div class="comment">
                            <div class="comment__images">
                                <img alt='' src="http://placehold.it/50x50">
                            </div>

                            <a href='#' class="comment__author"><!--span class="social-used fa fa-facebook"></span--><?php echo ucwords(helper::getUserName($dc['userid'])); ?></a>
                            <p class="comment__date"><?php echo helper::getTime($dc['time_created']); ?></p>
                            <p class="comment__message"><?php echo $dc['comment']; ?></p>
                            <!--a href='#' class="comment__reply">Reply</a-->
                        </div>

                      <?php
						}
                      ?>						
						<?php
						if($countcomment>10)
						{
						?>
	                        <div class="comment-more">
	                            <a href="javascript:;" class="watchlist">Show more comments</a>
	                        </div>
						<?php
						}
						?>
                    </div>
                    </div>
                </div>
            </div>

        </section>
        
        <div class="clearfix"></div>

        

	<!-- JavaScript-->
      
        <!-- Share buttons -->
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-525fd5e9061e7ef0"></script>
		<script type="text/javascript">
            $(document).ready(function() {
                init_MoviePage();
                init_MoviePageFull();
            });
		</script>