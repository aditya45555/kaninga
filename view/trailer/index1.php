<?php
	$db = Db::init();
	$contri = $db->contributors;
?>
<section class="container">
	<div class="col-xs-8 col-sm-8 col-md-3 body-menu">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<h6 style="font-size: 15px;" class="bold">TRAILER</h6>
			<div class="custom-container mouseWheelButtons" id="accordion">
		        <a style="padding-left: 0;" data-toggle="collapse" data-parent="#accordion" href="#collapse" class="bold"><h6>NOW PLAYING</h6></a>
			       <div style="margin-bottom: 20px" id="collapse" class="panel-collapse collapse"> 
			        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
			        <div class="carousel">
			            <ul class="gradasi">
			            	<?php
			            	foreach($datamovienw as $dtnw) {
			            		echo '<li>';
								echo '<div class="col-xs-12 col-sm-12 col-md-12 boxgradasi" style="margin:15px 0 15px 0;">';
			            		if (isset($dtnw['image'])){
			            			if(strlen(trim($dtnw['image'])) > 0)
										{
											$path_parts = pathinfo($dtnw['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".c150x80.".$ext;
											$image= CDN.'image/'.$url;
											echo '<a style="padding-left:0;">';
											echo '<div class="col-xs-6 col-sm-6 col-md-6"><img style="margin-left:0px" width="150" height="80" src="'.$image.'" alt=""/> </div>';
											$dtcontri = $contri->findOne(array('_id' => new MongoId($dtnw['contributor'])));
											echo '<div class="col-xs-6 col-sm-6 col-md-6" style="padding-left:10px; padding-top:10px;">'.$dtnw['name'].'<br><small>'.$dtcontri['name'].' </small></div>';
											echo '</a>';
										}
								echo '</div>';
								echo '</li>';
			            		}
			            	}
			            	?>
			            </ul>
			        </div>
			       <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
			     </div>  
			 </div>       
			 <div class="custom-container mouseWheelButtons" id="accordion">
		        <a style="padding-left: 0;" data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="bold"><h6>COMMING SOON</h6></a>
			       <div style="margin-bottom: 20px" id="collapse1" class="panel-collapse collapse"> 
			        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
			        <div class="carousel">
			            <ul class="gradasi">
			            	<?php
			            	foreach($datamoviecs as $dtcs) {
			            		echo '<li>';
								echo '<div class="col-xs-12 col-sm-12 col-md-12 boxgradasi" style="margin:15px 0 15px 0;">';
			            		if (isset($dtcs['image'])){
			            			if(strlen(trim($dtcs['image'])) > 0)
										{
											$path_parts = pathinfo($dtcs['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".c150x80.".$ext;
											$image= CDN.'image/'.$url;
											echo '<a style="padding-left:0;">';
											echo '<div class="col-xs-6 col-sm-6 col-md-6"><img style="margin-left:0px" width="150" height="80" src="'.$image.'" alt=""/> </div>';
											$dtcontri = $contri->findOne(array('_id' => new MongoId($dtcs['contributor'])));
											echo '<div class="col-xs-6 col-sm-6 col-md-6" style="padding-left:10px; padding-top:10px;">'.$dtcs['name'].'<br><small>'.$dtcontri['name'].' </small> </div>';
											echo '</a>';
										}
								echo '</div>';
								echo '</li>';
			            		}
			            	}
			            	?>
			            </ul>
			        </div>
			       <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
			     </div>  
			 </div>  
		    </div>
		</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-9 body-detail">
	</div>
	
</section>
<script type="text/javascript">
	$(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
    
</script>