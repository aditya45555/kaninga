	<?php
	$db=Db::init();
	$galerimoviedb = $db->galerimovies;
	?>
	
	<?php
	foreach($css as $c)
	{
	?>
		<link href="<?php echo $c?>" rel="stylesheet" />
	 <?php
	}
	 ?>
	 
	 <?php
	 foreach($js as $j)
	 {
	 ?>
	 	<script src="<?php echo $j?>"></script>
	 <?php
	 }
	 ?>
	 
	<!-- Search bar -->
	<div style="margin-top: 55px;" class="search-wrapper">
	    <div style="padding-top: 1.5%;" class="container container--add">
	        <form id='search-form' method='post' action="/trailer/index" class="search">
	            <input type="text" class="search__field" name="search" placeholder="Cari">
	            <select name="optionsearch" id="search-sort" class="search__sort" tabindex="0">
	                <option value="1" selected='selected'>Judul Film</option>
	            </select>
	            <button type='submit' class="btn btn-md btn--danger search__button">Cari Film</button>
	        </form>
	    </div>
	</div>
	
	<!-- Main content -->
	<section class="container">
	    <div class="col-sm-12">
	        <h2 class="page-heading">trailer</h2>
	
	        <!--<div class="tags-area wrap-select">
	            <div class="tags tags--unmarked">
	                <span class="tags__label">Sorted by:</span>
	                    <ul>
	                        <li class="item-wrap"><a href="#" class="tags__item">name</a></li>
	                        <li class="item-wrap"><a href="#" class="tags__item item-active">popularity</a></li>
	                        <li class="item-wrap"><a href="#" class="tags__item">date</a></li>
	                    </ul>
	            </div>
	        </div>!-->
	
	        <div class="trailer-wrapper">
	        	<?php
	        	foreach($datamovie as $dm)
	        	{
	        		$counttrailer = $galerimoviedb->count(array("movie" => trim($dm['_id']),"description" => "Videos"));
					$datagalerimovie = $galerimoviedb->find(array("movie" => trim($dm['_id']),"description" => "Videos"))->limit(3);
	        		$datagalerimoviehidden = $galerimoviedb->find(array("movie" => trim($dm['_id']),"description" => "Videos"))->skip(3);
	        	?>
		            <!-- Films trailers -->
		            <div class="trailer-block row">
		                <div class="col-sm-4 col-md-3">
		                    <div class="trailer">
		                        <p class="trailer__name"><?php echo $dm['name']?></p>
		                        <p class="trailer__number"><?php echo $counttrailer?> trailer</p>
		                    </div>
		                </div>
						<?php
	        			foreach($datagalerimovie as $dgm)
	        			{
	        			?>
			                <div class="col-sm-4 col-md-3">
			                    <a href='<?php echo $dgm['link']?>' class="trailer-sample">
			                        <img  alt='' src="http://img.youtube.com/vi/<?php  echo substr($dgm['link'], 32);?>/0.jpg" style="width:265px;height:160px">
			                    </a>
			                </div>
						<?php
		        		}
		        		?>
		        		<div class="hidden-content">
			        		<?php
			        		foreach ($datagalerimoviehidden as $dgmh ) 
			        		{
			        		?>
								 <div class="col-sm-4 col-md-3">
				                    <a href='<?php echo $dgmh['link']?>' class="trailer-sample">
				                        <img  alt='' src="http://img.youtube.com/vi/<?php  echo substr($dgmh['link'], 32);?>/0.jpg" style="width:265px;height:160px">
				                    </a>
					             </div>
							<?php
							}
							?>
						</div>
		        	<?php
		        	if($counttrailer>3)
		        	{
		        	?>
			        	<a href="#" class="trailer-btn">
				        	<span class="btn-circled"></span>
				        </a>
					<?php
					}
					?>		
		            </div>
		            <!--end films trailers-->
	            <?php
	            }
	            ?>
	        </div>
	
	        <div class="pagination paginatioon--full">
	               <?php echo $pagination?>
	        </div>
	
	    </div>
	</section>
	<div class="clearfix"></div>
	
	<!-- JavaScript-->
	<script type="text/javascript">
	    $(document).ready(function() {
	        init_Trailer();
	        $('.sbHolder').css({'right': '100px'});
	    });
	</script>
	
