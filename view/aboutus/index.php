<?php
$db = Db::init();
$schedule = $db->schedules;
$g = $db->genres;
?>
<section class="container">
	<div class="col-xs-6 col-sm-6 col-md-2 body-menu">
		<?php
        	foreach ($data as $dtp) {
				if (isset($dtp['image'])){
        			if(strlen(trim($dtp['image'])) > 0)
						{
							$path_parts = pathinfo($dtp['image']);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".150x80.".$ext;
							$image= CDN.'image/'.$url;
							echo '<div  class="imgbot">';
							echo '<img width="150" height="80" src="'.$image.'" alt=""/>';
							
						}
					}
				echo '<p style="font-size:15px; line-height:0; font-color:#4c4145; margin:10px; padding:0;align:left; margin-top:15px;">'.strtoupper($dtp['name']).'</p>';
				echo '<p style="font-size:10px; line-height:0; font-color:#4c4145; margin:10px; padding:0;align:left;">'.$dtp['address'].'</p>';
				echo '<p style="font-size:10px; line-height:0; font-color:#4c4145; margin:10px; padding:0;align:left;">'.$dtp['phone'].'</p>';
				echo '<p style="font-size:10px; line-height:0; font-color:#4c4145; margin:10px; padding:0;align:left;">'.$dtp['email'].'</p>';
				echo '</div>';
			}
        ?>
   		<div class="clear"></div>

	</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-10 body-detail">
		<div class="col-xs-12 col-sm-12 col-md-12" align="center">
		<?php
        	foreach ($data as $dtp) {
				if (isset($dtp['image'])){
        			if(strlen(trim($dtp['image'])) > 0)
						{
							$path_parts = pathinfo($dtp['image']);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".280x130.".$ext;
							$image= CDN.'image/'.$url;
							echo '<div align="center">';
							echo '<img width="280" height="130" src="'.$image.'" alt=""/>';
							echo '</div>';
						}
					}
			}
        ?>
        <br>
        <div>
        	<?php 
        		$colabout = $db->aboutus;
				$isi = $colabout->findone();
				
				echo '<p align="justify">'.$isi['content'].'</p>';
        	?>
        	<!--
			<p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
			when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, 
			but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets 
			containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			<p align="justify">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, 
			making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, 
			consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. 
			Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. 
			This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", 
			comes from a line in section 1.10.32.</p>-->
		</div>
        </div>
	</div>
</section>






