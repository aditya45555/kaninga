<?php
	$db = Db::init();
	$transaction=$db->transactions;
	$studio=$db->studios;
	$client=$db->clients;
	$movie = $db->movies;
	$curr= new currency();
	
	$name = 'SEMUA STUDIO';
	if(strlen(trim($studioid)) > 0)	{
		$nstudio = $studio->findone(array('_id' => new MongoId($studioid)));
		$name = strtoupper($nstudio['name']);
	}
?>
<div class="container" style="margin-top: 80px;">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12" style="margin:10px">
          <h2 class="pull-left">TRANSAKSI <?php echo $name; ?></h2>
            <form method="post" action="/transaction/indexmonth" class="pull-right form-inline">
			<div class="form-group">
	    		<label for="find-by">Find Studio</label>
	    			<select name="studio" class="form-control" id="findstudio">
	    				<option value="">All</option>
	    				<?php
	    					$query = array();
							if($_SESSION['tipe'] == 'Client')
								$query = array('clientid' => new MongoId($_SESSION['clientid']));
							$mstudio = $studio->find($query);
							
	    					foreach($mstudio as $stall)
	    					{
	    						$clientdata = $client->findOne(array('_id' => new MongoId(trim($stall['clientid']))));
	    				?>
	    						
	    						<option value="<?php echo $stall['_id']?>" <?php echo $stall['_id'] == $studioid ? ' selected="selected"' : '';?>><?php echo $clientdata['name']." - ".$stall['name']?></option>
	    				<?php		
	    					}
	    				?>
	    			</select>
	    	</div>
	    	<div class="form-group">
	    		<label for="find-by">Month</label>
    			<select name="month" class="form-control">
				<?php
				for($i=1;$i<=12;$i++)
				{
					if(intval($month) == $i)
						echo '<option value="'.$i.'" selected >'.helper::getMonthName($i).'</option>';
					else
						echo '<option value="'.$i.'" >'.helper::getMonthName($i).'</option>';
				}	
				?>
				</select>
	    	</div>
	    	<div class="form-group">
	    		<label for="find-by">Year</label>
    			<select name="year" class="form-control" id="findyear">
				<?php
				for($i=2015;$i<=2020;$i++)
				{
				?>		
					<option value="<?php echo $i;?>" <?php echo $i == $year ? ' selected="selected"' : '';?>><?php echo $i;?></option>
				<?php
				}
				?>
				</select>
	    	</div>
	      <input type="submit" class="btn btn-sm btn-primary" value="Search" id="btnfind"/>
         </form>
        </div>
        <div class="col-md-12">
        	<h4 class="pull-left">Month : <?php echo helper::getMonthName($month).' '.$year; ?></h4>
        	<div style="float:right;">
      	 		<h5>Total Sold Ticket : <?php echo $totalsoldticket;?> </h5>
	   	 		<h5>Total Rupiah : <?php echo $curr->curr($totalall);?> </h5>
      		</div>
        	<table class="table">
				<thead>
					<tr>
						<th>Date</th>
						<th>Sold Ticket</th>
						<th>Total Rupiah</th>
					</tr>
				</thead>
				<tbody>
					<?php
					for($i=1;$i<=date('t', $start);$i++) {
						echo "<tr>";						
						$ada = false;
						foreach($datatransaksi as $dat) {
							if(intval($dat['date']) == $i) {
								$ada = true;
								$d = $dat['date'];
								if(strlen(trim($dat['date'])) == 1)
									$d = '0'.$dat['date'];															
								echo '<td>'.$d.'</td>';
								echo '<td>'.$dat['ticket'].'</td>';
								echo '<td>Rp. '.number_format($dat['total'], 0,',','.').'</td>';
							}							
						}
						if(!$ada) {
							$d = $i;
							if(strlen(trim($i)) == 1)
								$d = '0'.$i;
							echo '<td>'.$d.'</td>';
							echo '<td>0</td>';
							echo '<td>Rp. 0</td>';
						}
						echo "</tr>";
					}
					?>
					
				</tbody>
			</table>
			
		</div>
      </div>

      <hr>

     
    </div> <!-- /container -->