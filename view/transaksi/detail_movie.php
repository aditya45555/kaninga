<?php
	$db = Db::init();
	$transactiondb= $db->transactions;
	if(isset($_GET['page'])&&$_GET['page']!=1)
	{
		$numpage=$_GET['page'];
		$i=(($limit*$numpage)-($limit-1));
	}
	else
	{
		$i=1;
	}
?>
<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12" style="margin:10px">
          <h2 class="pull-left"><?php echo $title?></h2>
          <a href="/report/dash_contributor"><p align="right">Back</p></a>
        </div>
        <div class="col-md-12">
        	<table class="table">
				<thead>
					<tr>
						<th>No.</th>
						<th>Studio</th>
						<th>Tiket Terjual</th>
						<th>Total Penjualan</th>
						<th>Total Profit</th>
					<tr>
				</thead>
				<tbody>
					<?php
						foreach ($datastudio as $d) 
  						{
  							$totalsales = 0;
							$totalsoldticket = 0;
								
  							$datatrx = $transactiondb->find(array("studio" => trim($d['_id']),"movie"=> $movie));
  							foreach($datatrx as $dtx){
  								$pr = abs($dtx['price']);	
								$totalsales += (intval($pr)-(intval($pr)*$dtx['diskon']/100))*intval($dtx['qty']);
								$totalsoldticket+=$dtx['qty'];
  							}
							$totalprofit = $totalsales*(0.1);
  							echo '<tr>
  									<td align="center">'.$i.'</td>
  									<td>'.$d['name'].'</td>
  									<td>'.$totalsoldticket.'</td>
									<td>'.number_format($totalsales, 0,',','.').'</td>
									<td>'.number_format($totalprofit, 0,',','.').'</td>
									
								<tr>';
						$i++;
						}
					?>
				</tbody>
			</table>
			<div id="pagination-platform">
				<?php echo $pagination ?> 
	  		</div>
		</div>
      </div>
   
    </div> <!-- /container -->