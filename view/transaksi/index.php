<?php
	$db = Db::init();
	$user=$db->users;
	$movie=$db->movies;
	$konsumen=$db->konsumens;
	$studio=$db->studios;
	$curr= new currency();
	if(isset($_GET['page'])&&$_GET['page']!=1)
	{
		$numpage=$_GET['page'];
		$i=(($limit*$numpage)-($limit-1));
	}
	else
	{
		$i=1;
	}
?>
<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12" style="margin:10px">
          <h2 class="pull-left">TRANSACTION RANGE</h2>
            <form method="post" action="/transaksi/index" class="pull-right form-inline">
	  		<div class="form-group">	  			
	    		<div id="finddate">
	    			<label for="find-by">Transaction Date </label>
	    			<input type="text" class="form-control datepicker" name="start" id="fromdate" placeholder="From Date" value="<?php echo $start; ?>" />
	    			<label>to</label>
	    			<input type="text" class="form-control datepicker" name="end" id="todate" placeholder="To Date" value="<?php echo $end; ?>"/>
	    		</div>
	    	</div>
	    	
	      <input type="submit" class="btn btn-sm btn-primary" value="Search" id="btnfind"/>
         </form>
        </div>
       
        <div class="col-md-12">
        	<h4 class="pull-left">From : <?php echo $start; ?> to <?php echo $end; ?></h4>
        	<table class="table">
				<thead>
					<tr>
						<th>No.</th>
						<th>CASHIER</th>
						<th>Movie</th>
						<th>Studio</th>
						<th>Date</th>
						<th>QTY Ticket</th>
						<th>Price</th>
						<th>Total</th>
						<th>Konsumen</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($data as $d) 
  						{
  							$userdata = $user->findOne(array('_id' => new MongoId(trim($d['user']))));
							$moviedata = $movie->findOne(array('_id' => new MongoId(trim($d['movie']))));
							if(trim($d['konsumen'])!=null)
							{
								$konsumendata = $konsumen->findOne(array('_id' => new MongoId(trim($d['konsumen']))));
								$phonekonsumen = $konsumendata['name'].' ('.$konsumendata['handphone'].')';	
							}
							else
							{
								$phonekonsumen = "";	
							}
							if(isset($d['diskon']))
							{
								$diskon=$d['diskon'];
							}
							else
							{
								$diskon=0;
							}
							
							if(isset($d['voucher']))
							{
								$nilaivoucher=0;
								foreach($d['voucher'] as $voucher)
								{
									$nilaivoucher = $voucher['nilai']; 
								}
								
							}
							else
							{
								$nilaivoucher = 0;
							}
							
							$totalprice  = $d['price']*$d['qty'];
							$totaldiskon = ($totalprice*$diskon)/100;
							$totaltax    = ($totalprice*$moviedata['tax'])/100;
							$total = (($totalprice+$totaltax)-$totaldiskon)-$nilaivoucher;
							
							$studioname = "";
							if(strlen($d['studio']) > 1)
							{
								$studiodata = $studio->findOne(array('_id'=> new MongoId(trim($d['studio']))));
								if(isset($studiodata['_id']))
									$studioname = $studiodata['name'];
							}
  							
  							echo '<tr>
  									<td>'.$i.'</td>
									<td>'.$userdata['name'].'</td>
									<td>'.$moviedata['name'].'</td>
									<td>'.$studioname.'</td>
									<td>'.date("Y-m-d H:m:s",$d['transactiondate']).'</td>
									<td>'.$d['qty'].'</td>
									<td>Rp. '.number_format($d['price'], 0,',','.').'</td>
									<td>Rp. '.number_format($total, 0,',','.').'</td>
									
									<td>'.$phonekonsumen.'</td>
								<tr>';
							$i++;
						}
					?>
					
				</tbody>
			</table>
			<div id="pagination-platform">
				<?php echo $pagination ?> 
	  		</div>
		</div>
      </div>

      <hr>

      <footer>
        <p>&copy; Company 2014</p>
      </footer>
    </div> <!-- /container -->