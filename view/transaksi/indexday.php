<?php
	$db = Db::init();
	$movie = $db->movies;
	$transaction=$db->transactions;
	$studio=$db->studios;
	$client=$db->clients;
	$schedule = $db->schedules;
	$curr= new currency();
	if(isset($_GET['page'])&&$_GET['page']!=1)
	{
		$numpage=$_GET['page'];
		$i=(($limit*$numpage)-($limit-1));
	}
	else
	{
		$i=1;
	}
?>
<div class="container" style="margin-top: 80px;">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12" style="margin:10px">
          <h2 class="pull-left">TRANSACTION DAY</h2>
            <form method="post" action="/transaksi/indexday" class="pull-right form-inline">
			<div class="form-group">
	    		<label for="find-by">Find Studio</label>
	    			<select name="studio" class="form-control" id="findstudio">
	    				<option value="">All</option>
	    				<?php
	    					$query = array();
							if($_SESSION['tipe'] == 'Client')
								$query = array('clientid' => new MongoId($_SESSION['clientid']));
							$mstudio = $studio->find($query);
							
	    					foreach($mstudio as $stall)
	    					{
	    						$clientdata = $client->findOne(array('_id' => new MongoId(trim($stall['clientid']))));
	    				?>
	    						
	    						<option value="<?php echo $stall['_id']?>" <?php echo $stall['_id'] == $studioid ? ' selected="selected"' : '';?>><?php echo $clientdata['name']." - ".$stall['name']?></option>
	    				<?php		
	    					}
	    				?>
	    			</select>
	    	</div>
	  		<div class="form-group">
	    		<div id="findtext" style="display: none">
	    			<input type="text" class="form-control" name="find" id="find" placeholder="Keyword" />
	    		</div>
	    		<div id="finddate">
	    			<input type="text" class="form-control datepicker" name="date" id="fromdate" placeholder="From Date" value="<?php echo $date; ?>" />
	    		</div>
	    	</div>
	    	
	      <input type="submit" class="btn btn-sm btn-primary" value="Search" id="btnfind"/>
         </form>
        </div>
       
        <div class="col-md-12">
        	<h4 class="pull-left">Date : <?php echo strtoupper($date)?></h4>
        	<div style="float:right;">
      	 		<h5>Total Sold Ticket : <?php echo $totalsoldticket;?> </h5>
	   	 		<h5>Total Rupiah : <?php echo $curr->curr($totalall);?> </h5>
      		</div>
        	<table class="table">
				<thead>
					<tr>
						<th>No.</th>
						<?php
						if(strlen($studioid)<1){
						?>
						<th>Studio</th>
						<?php	
						}
						?>
						<th>Date</th>
						<th>Schedule Time</th>
						<th>QTY Ticket</th>
						<th>Price</th>
						<th>Diskon</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($data as $d) 
  						{
  							if(strlen($studioid)<1){
  								$studiodata = $studio->findOne(array("_id"=> new Mongoid(trim($d['studio']))));
  							}
							$scheduledata = $schedule->findone(array('_id'=>new MongoId(trim($d['schedule']))));
  							$moviedata = $movie->findone(array('_id' => new MongoId(trim($d['movie']))));
  							$v = 0;
							foreach($d['voucher'] as $vv)
								$v = intval($vv['nilai']);
							
							$price = intval(($d['price']*$d['qty'])-(($d['price']*$d['qty'])*($d['diskon']/100)));
							$tprice = intval($price-($price*($moviedata['tax']/100)));
							$total = intval($tprice-$v);
  							echo '<tr>
  									<td>'.$i.'</td>';
									
									if(strlen($studioid)<1){
										echo "<td>".$studiodata['name']."</td>";	
									}									
							echo	'<td>'.date('d-m-Y H:i:s', $d['transactiondate']).'</td>
									 <td>'.date('d-m-Y', $scheduledata['scheduledate'])." ".$scheduledata['scheduletime'].'</td>
									<td>'.$d['qty'].'</td>'.
										
								'<td>Rp. '.number_format($d['price'], 0,',','.').'</td>
								<td>'.$d['diskon']."%".'</td>	
									<td>Rp. '.number_format($total, 0,',','.').'</td>
									<td>
										
									</td>										
								<tr>';
							$i++;
						}
					?>
					
				</tbody>
			</table>
			<?php echo $pagination; ?>
		</div>
      </div>

      <hr>

      <footer>
        <p>&copy; Company 2014</p>
      </footer>
    </div> <!-- /container -->