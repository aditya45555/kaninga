<?php
$db = Db::init();
$schedule = $db->schedules;
$g = $db->genres;
?>
<section class="container">
	<div class="col-xs-12 col-sm-12 col-md-4 body-menu">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 style="font-size: 15px;" class="bold">NOW PLAYING IN</h6>
			<div class="custom-container mouseWheelButtons">
		        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
		        <div class="carousel">
		            <ul>
		            	<?php
		            	foreach($datakota as $dts) {
		            		$cls = ($dts['id'] == trim($idkota)) ? ' active' : '';
		            		echo '<li data-value="'.$dts['id'].'"><a class="kota '.$cls.'" href="/movieplaying/index?kota='.$dts['id'].'">'.$dts['name'].'</a></li>';
		            	}
		            	?>
		            </ul>
		        </div>
		        <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
		        <div class="clear"></div>
		    </div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 id="city" style="font-size: 15px;" class="bold"><?php echo $name; ?></h6>
			<div class="custom-container mouseWheelButtons2">
		        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
		        <div id="menu" class="carousel">
		            <ul>		            	
		            	<?php
						$n=0;
		            	foreach($datastudio as $dtsd) {		            		
		            		$cls = ($dtsd['_id'] == trim($idstudio)) ? 'active' : 'collapsed';
		            		echo '<li id="accordion">';
	            			echo '<a style="padding-left: 0;" data-nmr="'.$n.'" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$n.'" class="subdistrict '.$cls.'">'.$dtsd['subdistrict'].'</a>';
							echo '</li>';
							$clsp = ($dtsd['_id'] == trim($idstudio)) ? 'in' : 'collapse';
		            		echo '<div style="margin-bottom: 20px" id="collapse'.$n.'" class="panel-collapse '.$clsp.'">';
							echo '<p style="font-size: 11px; margin-bottom: 0; margin-top:-15px;color: #86736b;">'.$dtsd['name'].'</p>';
							$end = strtotime("+7 day", time());
							$q = array(
								'studio' => trim($dtsd['_id']),
								'scheduledate' => array('$gte' => time(), '$lt' => $end)//1456678801//time()
							);
							$ss = $schedule->find($q);
							foreach($datamovie as $dv) {
								$ada = false;
								$jadwal = '';
								foreach($ss as $ds) {
									if($dv['_id'] == $ds['movie']) {
										$ada = true;
										$jadwal .= '<p style="text-transform: uppercase; font-size:12px; margin-bottom: -20px; line-height: 40px; padding-left: 10px;color: #86736b;">'.date('F d', $ds['scheduledate']).' - '.$ds['scheduletime'].' WIB</p>';
									}
								}
								if($ada) {
									$clsm = ($dv['_id'] == $id) ? 'active' : '';
									echo '<a class="'.$clsm.'" href="/movieplaying/index?kota='.$idkota.'&studio='.$dtsd['_id'].'&id='.$dv['_id'].'" style="text-transform: uppercase; cursor: pointer; padding-left: 10px; min-width: 80%">'.$dv['name'].'</a>';
									if (isset($dv['image']))
									{
										if(strlen(trim($dv['image'])) > 0)
										{
											$path_parts = pathinfo($dv['image']);
											$f = $path_parts['filename'];
											$ext = $path_parts['extension'];
											$url = $f.".80x120.".$ext;
											$image= CDN.'image/'.$url;
											echo '<img style="margin-left:10px" width="80" height="120" src="'.$image.'" alt=""/>';
										}
									}									
									//echo $jadwal;
								}
							}
		            		echo '</div>';		            	 	
							$n++;							
		            	}
		            	?>		            	
		            </ul>		            
		        </div>
		        <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
		        <div class="clear"></div>
		    </div>
		</div>
	</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-8 body-detail">
		<?php if(count($data) > 0) { ?>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="col-xs-9 col-sm-9 col-md-9">
				<h6 class="bold"><?php echo isset($data['name']) ? $data['name'] : '&nbsp;'; ?></h6>
				<ul class="summary">
					<li>
						<span class="key">genre</span>
						<?php
						$genre = '&nbsp;';
						if(isset($data['genre'])) {
							$gg = $g->findone(array('_id' => new MongoId($data['genre'])));
							$genre = $gg['name'];
						}
						?>
						<span class="val"><?php echo $genre; ?></span>
					</li>
					<li>
						<span class="key">produser</span>
						<span class="val"><?php echo isset($data['produser']) ? $data['produser'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">sutradara</span>
						<span class="val"><?php echo isset($data['sutradara']) ? $data['sutradara'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">penulis</span>
						<span class="val"><?php echo isset($data['penulis']) ? $data['penulis'] : '&nbsp;'; ?></span>
					</li>
					<li>
						<span class="key">produksi</span>
						<span class="val"><?php echo isset($data['produksi']) ? $data['produksi'] : '&nbsp;'; ?></span>
					</li>
				</ul>
				<h6 class="bold">CAST</h6>
				<ul class="cast">
					<?php
						if(isset($data['cast'])) {
							if(strlen(trim($data['cast'])) > 0) {
								if(strpos($data['cast'], ',') !== false) {
									$cs = explode(',', $data['cast']);
									foreach($cs as $ds) {
										if(strlen(trim($ds)) > 0)
										echo '<li><span class="desc">'.$ds.'</span></li>';
									}
								}
								else
									echo '<li><span class="desc">'.$data['cast'].'</span></li>';
							}
						} 
					?>
				</ul>				
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 duration">
				<h5 class="bold"><?php echo isset($data['duration']) ? helper::timetosecond($data['duration']) : '0'; ?></h5>
				<p style="margin-top: -20px; text-align: center;" class="synopsis"><?php echo strlen(trim($data['duration']) > 0) ? 'Minutes' : '&nbsp'; ?></p>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12 col-sm-12 col-md-12 margin10">
				<h6 class="bold">SYNOPSIS</h6>
				<p class="synopsis"><?php echo isset($data['synopsis']) ? helper::limitString($data['synopsis']) : '&nbsp'; ?></p>
				<ul class="body-detail-menu">
					<!--li><a class="bold" href="/trailer/index?id=<?php echo $data['_id']; ?>">trailer</a></li>
					<li><a class="bold" href="/booking/index?id=<?php echo $data['_id']; ?>">buy ticket</a></li-->
				</ul>
			</div>		
			<div class="col-xs-12 col-sm-12 col-md-12 margin10">	
				<?php 
	        		$colstd = $db->studios;
					$findstd = $colstd->findOne(array('_id' => new MongoId (trim($idstudio))));
					echo '<h6 class="bold"> JADWAL '.$findstd['name'].' </h6>';
					echo '<p class="synopsis"> '.$findstd['address'].'</p>';
				?>
			
				<hr style="border-top: 1px solid #826c64; margin-top: 0;">
				<table cols="2">
				<?php
					$end = strtotime("+7 day", time());
					$findsch = $schedule->find(array('studio' => trim($idstudio), 'movie' => $id,'active' => '1' ,'scheduledate' => array('$gte' => time(), '$lt' => $end)))->sort(array('scheduledate' => 1));
					foreach ($findsch as $valuesch) {
						echo '<tr class="synopsis" style="margin-top:0; margin-bottom:0;">
						     	<td>'.date("F j, Y",$valuesch['scheduledate']).' </td>
						     	<td>'.$valuesch['scheduletime'].'</td> 
						     </tr>';
					}
				?>
				</table>
			</div>
			<!-- end jadwal -->	
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<?php
			if (isset($data['image']))
			{
				if(strlen(trim($data['image'])) > 0)
				{
					$path_parts = pathinfo($data['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".350x500.".$ext;
					$image= CDN.'image/'.$url;
					echo '<img style="margin-left:10px" width="350" height="500" src="'.$image.'" alt=""/>';
				}
			}
			?>
		</div>
		<?php } else { ?>
			<h2>No Data Available</h2>
		<?php } ?>
	</div>
</section>
<script type="text/javascript">
	$(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
</script>