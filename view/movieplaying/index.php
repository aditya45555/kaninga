 <?php
 $db=Db::init();
 $scheduledb = $db->schedules;
 $genredb = $db->genres;
 $contributordb = $db->contributors;
 $votemoviedb = $db->rating_movies;
 $studiodb = $db->studios;
 $galeridb = $db->galerimovies;
 $commentdb = $db->commentmovies;
 $moviedb = $db->movies;
 $towndb = $db->place_towns;
 $help = new helper();
 $helpdata = $help->getRatingDisplay(); 
 ?>
  
 <?php
 foreach($js as $j)
 {
 ?>
 <script src="<?php echo $j?>"></script>
 <?php	
 }
 ?> 	
        <!-- Main content -->
        <section style="margin-top: 5%" class="container">
            <div class="col-sm-8 col-md-9">
                <h2 class="page-heading">Film Tayang</h2>
                <div class="alert alert-success">
  					Rating Movie Diterima
				</div>
                <div class="select-area">
                    <!--<form id='select' class="select" method='get'>
                          <select name="select_item" class="select__sort" tabindex="0" >
                          	<option value="all">All</option>
                            <?php
                            for($i=0;$i<$countdatastudiodistinct;$i++)
							{
								$datatown = $towndb->findOne(array("_id" => new MongoId(trim($datastudiodistinct[$i]))));		
                            ?>
                            	<option value="<?php echo $datatown["_id"]?>"><?php echo $datatown['name']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </form>!-->

                    <div class="datepicker">
                      <span class="datepicker__marker"><i class="fa fa-calendar"></i>Tanggal</span>
                      <input type="text" id="datepicker" value='<?php echo date("m/d/Y",$datenow)?>' class="datepicker__input">
                    </div>

                    <form class="select select--cinema" method='get' id="testform">
                          <select id="studioid" name="select_item" class="select__sort"  tabindex="0">
                          	<option value="all" selected="selected">Semua</option>
                             <?php
                             foreach($datastudio as $ds)
                             {
                            ?>
                            	<option value="<?php echo $ds['_id']?>"  <?php echo $ds['_id'] == $studioid ? ' selected="selected"' : '';?>"><?php echo $ds['name']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </form>

                   <!-- <form class="select select--film-category" method='get'>
                          <select name="select_item" class="select__sort" tabindex="0">
                          	<option value="all" selected="selected">All</option>
                             <?php
                            foreach($datagenre as $ds)
                            {
                            ?>
                            	<option value="<?php echo $ds['_id']?>"><?php echo $ds['name']?></option>
                            <?php
                            }
                            ?>
                        </select>
                   </form>!-->

                </div>

                <div class="tags-area">
                    <div class="tags tags--unmarked">
                        <span class="tags__label">Sorted by:</span>
                            <ul>
                                <li class="item-wrap"><a href="#" class="tags__item item-active" data-filter='all'>all</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='release'>release date</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='popularity'>popularity</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='comments'>comments</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='ending'>ending soon</a></li>
                            </ul>
                    </div>
                </div>
                
                <?php
                for($i=$skip;$i<($page*$limit);$i++)
                {
					if(isset($iddatamovie[$i]))
					{	
						$dm = $moviedb->findOne(array("_id" => new MongoId(trim($iddatamovie[$i]))));			
	                	$genre=$genredb->findOne(array("_id" => new MongoId(trim($dm['genre']))));
						$contributor=$contributordb->findOne(array("_id" => new MongoId(trim($dm['contributor']))));
						$votemovie = $votemoviedb->find(array("movie" =>trim($dm['_id'])));
						$galerifoto = $galeridb->count(array("movie" => trim($dm['_id']),"description" => "Images" ));
						$galerivideo = $galeridb->count(array("movie" => trim($dm['_id']),"description" => "Videos" ));
						$comment = $commentdb->find(array("movie" => trim($dm['_id'])));
	                	$image="";
						if (isset($dm['image']))
						{
							if(strlen(trim($dm['image'])) > 0)
							{
								$path_parts = pathinfo($dm['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".c380x600.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	                ?>
	                <!-- Movie preview item -->
	                	<div class="movie movie--preview release">
	                     <div class="col-sm-5 col-md-3">
	                            <div class="movie__images">
	                                <img alt='' src="<?php echo $image?>">
	                            </div>
	                            <div class="movie__feature">
	                                <a href="#" class="movie__feature-item movie__feature--comment"><?php echo $comment->count()?></a>
	                                <a href="#" class="movie__feature-item movie__feature--video"><?php echo $galerivideo?></a>
	                                <a href="#" class="movie__feature-item movie__feature--photo"><?php echo $galerifoto?></a>
	                            </div>
	                    </div>
	                    <div class="col-sm-7 col-md-9">
	                            <a href='/moviedetail/index?movieid=<?php echo $dm['_id']?>' class="movie__title link--huge"><?php echo $dm['name']?></a>
	                            <p class="movie__time"><?php echo $dm['duration']?> Jam</p>
	                           
	                            <p class="movie__option"><strong>Genre: </strong><a href="#"><?php echo $genre['name']?></a></p>
	                            <p class="movie__option"><strong>Tanggal Rilis: </strong><?php echo date("d/M/Y",$dm['receiptdate'])?></p>
	                            <p class="movie__option"><strong>Contributor: </strong><a href="#"><?php echo $contributor['name']?></a></p>
	                         
	                           <!-- <div class="movie__btns">
	                                <a href="#" class="btn btn-md btn--warning">book a ticket <span class="hidden-sm">for this movie</span></a>
	                                <a href="#" class="watchlist">Add to watchlist</a>
	                           </div>!-->
	                            <div class="preview-footer">
	                               <!--div class="movie__rate"><div class="score" id="score" movieid=<?php //echo trim($dm['_id'])?> style="display: <?php //echo $helpdata['displayrating']?>"></div><div style="margin-top:35px "></div><span class="movie__rate-number" style="display:<?php //echo $helpdata['displayrating']?>"><?php //echo $votemovie->count();?> votes</span> <span class="movie__rating" style="left: <?php //echo $helpdata['leftrating']?>"><?php //echo helper::getRatingMovie($dm['_id'])?></span></div-->
	                               <a href="#" class="movie__show-btn">Detail Tayang</a>
	                            </div>
	                    </div>
	
	                    <div class="clearfix"></div>
	                    
	                    <!-- Time table (choose film start time)-->
		                    <div class="time-select">
		                    	<?php
	                   			foreach($datastudiofilter as $st)
	                   			{
	                   				$schedule=$scheduledb->find(array("movie" => trim($dm['_id']),"active" => "1","scheduledate" => $datenow+1,"studio" => trim($st['_id'])));
	                   				$scheduleone=$scheduledb->findOne(array("movie" => trim($dm['_id']),"active" => "1","scheduledate" => $datenow+1,"studio" => trim($st['_id'])));
									
									if(strlen($scheduleone['_id'])>0)
									{
	                   			?>
			                        <div class="time-select__group">
			                            <div class="col-sm-4">
		                   				<?php	
			                   			echo "<p class='time-select__place'>".$st['name']."</p>";				
		                   				?>
			                            </div>
			                            <ul class="col-sm-8 items-wrap">
			                                <?php
			                                foreach($schedule as $sch)
			                                {
			                                ?>
			                                	<li class="time-select__item" data-time='<?php echo $sch['scheduletime']?>'><?php echo $sch['scheduletime']?></li>
			                                <?php	
			                                }
			                                ?>
			                            </ul>
			                        </div>
		                    	<?php
									}
								}
	                		?>
	                    </div>
	                
	                <!-- end time table-->
                </div>
                <!-- end movie preview item -->
	           	<?php
	           	}	
                }
                ?> 
                <div class="coloum-wrapper">
                    <div class="pagination paginatioon--full">
                            <?php echo $pagination ?> 
                    </div>
                </div>

            </div>
                    
            <aside class="col-sm-4 col-md-3">
                <div class="sitebar">
                    <div class="category category--discuss category--count marginb-sm mobile-category ls-cat">
                        <h3 class="category__title">Film <br><span class="title-edition">Yang Sedang di Diskusikan</span></h3>
                        <ol>
                            <?php
                            foreach($datacomment as $dc)
                            {
                            	$movie = $moviedb->findOne(array("_id" => new MongoId(trim($dc['movie']))));
                            ?>
                            	<li><a href="#" class="category__item"><?php echo $movie['name'];?></a></li>
                            <?php	
                            }
                            ?>
                        </ol>
                    </div>

                    <div class="category category--cooming category--count marginb-sm mobile-category rs-cat">
                        <h3 class="category__title">coming soon<br><span class="title-edition">film</span></h3>
                        <ol>
                        	<?php
                        	foreach ($datacommingsoon as $dcs) 
                        	{
                        		$datascheduleone = $scheduledb->findOne(array("movie" => trim($dcs['_id'])));
								if(!isset($datascheduleone))
								{
							?>
									<li><a href="/moviedetail/index?movieid=<?php echo $dcs['_id']?>" class="category__item"><?php echo $dcs['name']?></a></li>
							<?php		
								}
							}
                        	?>
                        </ol>
                    </div>
                </div>
            </aside>
        </section>
        
        <div class="clearfix"></div>
		<script type="text/javascript">
            $(document).ready(function() {
                init_MovieList();
            });
		</script>
