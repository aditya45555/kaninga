        <?php
        $db = Db::init();
		$galeristudio = $db->galeristudios;
		$towndb = $db->place_towns;
        ?>
        
        <?php
		foreach($js as $j)
		{
		?>
		 	<script src="<?php echo $j?>"></script>
		<?php	
		}
		?> 	
        <!-- Main content -->
        <section style="margin-top: 5%" class="container">
            <div class="col-sm-12">
                <h2 class="page-heading">bioskop</h2>

                <div class="tags-area tags-area--thin">
                    <form id='select' class="select" method='get'>
                          <select id="cityid" name="select_item" id="select-sort" class="select__sort" tabindex="0">
                          	<option value="all">Semua</option>
                            <?php
                            for($i=0;$i<$countdatastudiodistinct;$i++)
                            {
                            	$town = $towndb->findOne(array("_id" => new MongoId($datastudiodistinct[$i])));
                            ?>
                            	<option value="<?php echo $datastudiodistinct[$i]?>"  <?php echo $town['_id'] == $cityid ? ' selected="selected"' : '';?>><?php echo $town['name']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </form>

                    <!--<div class="tags tags--unmarked tags--aside">
                        <span class="tags__label">Sorted by:</span>
                            <ul>
                                <li class="item-wrap"><a href="#" class="tags__item item-active" data-filter='all'>all</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='name'>name</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item" data-filter='popularity'>popularity</a></li>
                            </ul>
                    </div>!-->
                </div>
                
                <div class="cinema-wrap">
                	<?php
                     $i=0;
                     foreach($datastudio as $ds)
                     {
                     	$datagaleristudioone = $galeristudio->findOne(array("studio" => trim($ds['_id'])));
	                 	$image="";
						if (isset($datagaleristudioone['image']))
						{
							if(strlen(trim($datagaleristudioone['image'])) > 0)
							{
								$path_parts = pathinfo($datagaleristudioone['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".525x525.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	                 	
	                 	if($i%4==0)
	                    {
	                     	echo "<div class='row'>";        
						}
								
                    ?>
	                    <div class="col-xs-6 col-sm-3 cinema-item">
	                        <div class="cinema">
	                            <a href='/studiodetail/index?studioid=<?php echo $ds['_id']?>' class="cinema__images">
	                                <img alt='' src="<?php echo $image?>" style="width: 262px;height: 262px;">
	                                <!--span class="cinema-rating"><?php //echo $ds['rating']?></span-->
	                            </a>
	                            <a href="/studiodetail/index?studioid=<?php echo $ds['_id']?>" class="cinema-title"><?php echo $ds['name']?></a>
	                        </div>
	                    </div>
                <?php
	                    if($i%4==0)
	                    {
	                       echo "</div>";
	                    }
	                    $i++;
				}
                ?>
                    </div>
                    <div class="pagination paginatioon--full">
                           <?php echo $pagination?>
                    </div>
            </div>
        </section>
        <div class="clearfix"></div>
		<!-- JavaScript-->
        <script type="text/javascript">
            $(document).ready(function() {
                init_CinemaList();
            });
		</script>
