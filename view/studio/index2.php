<?php
$db = Db::init();
$studio = $db->studios;
$schedule = $db->schedules;
$movie = $db->movies;
$mv = $movie->find();
?>
<section class="container">
	<div class="col-xs-12 col-sm-12 col-md-4 body-menu">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 style="font-size: 15px;" class="bold">theaters</h6>
			<div class="custom-container mouseWheelButtons">
		        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
		        <div class="carousel">
		            <ul>
		            	<?php
		            	foreach($datakota as $dts) {
		            		$cls = ($dts['id'] == trim($idkota)) ? ' active' : '';
		            		echo '<li data-value="'.$dts['id'].'"><a class="kota '.$cls.'" href="/studio/index?kota='.$dts['id'].'">'.$dts['name'].'</a></li>';
		            	}
		            	?>
		            </ul>
		        </div>
		        <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
		        <div class="clear"></div>
		    </div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<h6 id="city" style="font-size: 15px;" class="bold"><?php echo $name; ?></h6>
			<div class="custom-container mouseWheelButtons2">
		        <a href="#" class="prev"><i class="fa fa-sort-desc"></i></a>
		        <div id="menu" class="carousel">
		            <ul>		            	
		            	<?php
		            	$mstd = $studio->find(array('towns' => $idkota))->sort(array('name' => 1));
						$n=0;
		            	foreach($mstd as $dts) {
		            		$cls = ($dts['_id'] == trim($data['_id'])) ? ' active' : '';
		            		echo '<li>';
	            			echo '<a style="padding-left: 0;" href="/studio/index?kota='.$idkota.'&id='.$dts['_id'].'" class="studio '.$cls.'">'.$dts['subdistrict'].'</a>';
							echo '</li>';		            	 	
							$n++;							
		            	}
		            	?>		            	
		            </ul>		            
		        </div>
		        <a href="#" class="next"><i class="fa fa-sort-asc"></i></a>
		        <div class="clear"></div>
		    </div>
		</div>
	</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-8 body-detail">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<h6 class="bold"><?php echo $data['name']; ?></h6>
				<ul class="summary">
					<li>
						<span class="key">address</span>
						<span class="val"><?php echo $data['address']; ?></span>
					</li>
					<li>
						<span class="key">phone</span>
						<span class="val"><?php echo $data['phone']; ?></span>
					</li>
					<li>
						<span class="key">capacity</span>
						<span class="val"><?php echo $data['quota']; ?></span>
					</li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<h6 class="bold">SCHEDULE</h6>
				<ul class="summary">
					<li>
						<span class="key">monday234</span>
						<span class="val">16:00 WIB</span>
					</li>
					<li>
						<span class="key">tuesday</span>
						<span class="val">16:00 WIB</span>
					</li>
					<li>
						<span class="key">wednesday</span>
						<span class="val">16:00 WIB</span>
					</li>
					<li>
						<span class="key">thursday</span>
						<span class="val">16:00 WIB</span>
					</li>
					<li>
						<span class="key">friday</span>
						<span class="val">16:00 WIB AND 20:00 WIB</span>
					</li>
					<li>
						<span class="key">saturday</span>
						<span class="val">16:00 WIB AND 20:00 WIB</span>
					</li>
					<li>
						<span class="key">sunday</span>
						<span class="val">16:00 WIB AND 20:00 WIB</span>
					</li>
				</ul>				
			</div>		
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<?php
			if (isset($data['image']))
			{
				if(strlen(trim($data['image'])) > 0)
				{
					$path_parts = pathinfo($data['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".".$ext;
					$image= CDN.'image/'.$url;
					echo '<img style="margin-left:10px" width="100%" src="'.$image.'" alt=""/>';
				}
			}
			?>
		</div>
		<input type="hidden" id="studio" value="<?php echo $data['_id']; ?>">
		<div class="col-md-12">
			<div id="map" style="width: 100%; height: 400px"></div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
</script>