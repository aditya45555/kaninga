	<?php
	$db=Db::init();
	$scheduledb = $db->schedules;
	$moviedb = $db->movies;
	$genredb = $db->genres;
	$contributordb = $db->contributors;
	$datamovie = $moviedb->find();
	
	?>	
	
   <?php
   foreach($css as $c)
   {
   ?>
		<link href="<?php echo $c?>" rel="stylesheet" /> 
   <?php			
   }
   ?>
	
	<?php
	foreach($js as $j)
	{
	?>
		<script src="<?php echo $j?>"></script>
	<?php
	}
	?>
	<!-- Main content -->
	<section style="margin-top: 5%" class="cinema-container">
	<input type="hidden" id="studioid" value="<?php echo $studioid?>" />
	<div class="cinema cinema--full">
	<p class="cinema__title"><?php echo $datastudio['name']?></p>
	<div class="cinema__rating"><?php echo $datastudio['rating']?></div>
	<div class="cinema__gallery">
	<div class="swiper-container">
	  <div class="swiper-wrapper" >
	 
	  <?php
	  foreach($datagaleristudio as $dgs)
	  {
	  	$image="";
		if (isset($dgs['image']))
		{
			if(strlen(trim($dgs['image'])) > 0)
			{
				$path_parts = pathinfo($dgs['image']);
				$f = $path_parts['filename'];
				$ext = $path_parts['extension'];
				$url = $f.".380x380.".$ext;
				$image= CDN.'image/'.$url;
			}
		}
	  ?>
		  <!--Slide-->
		  <div class="swiper-slide" > 
			<img alt='' src="<?php echo $image?>">
		  </div>
		  
	  <?php
	  }
	  ?>
	  </div>
	</div>
	</div>
	<div class="cinema__info">
		<p class="cinema__info-item"><strong>Alamat:</strong> <?php echo $datastudio['address']?></p>
		<p class="cinema__info-item"><strong>Telepon:</strong> <?php echo $datastudio['phone']?></p>
		<p class="cinema__info-item"><strong>Email:</strong> <?php echo $datastudio['email']?></p>
	</div>
	</div>
	
	<div class="share share--centered">
		<div class="addthis_toolbox addthis_default_style ">
			<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
			<a class="addthis_button_tweet"></a>
			<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
		</div>
	</div>
	
	<div class="tabs tabs--horisontal">
	  <!-- Nav tabs -->
	  <div class="container">
	  <ul class="nav nav-tabs" id="hTab">
		<!--<li ><a href="#movie1" data-toggle="tab">Movies</a></li>!-->
		<li><a href="#comment1" data-toggle="tab">Comment</a></li>
		<li class="active"><a href="#map1" data-toggle="tab">Peta</a></li>
	  </ul>
	 </div>
	
	  <!-- Tab panes -->
	  <div class="tab-content">
	<div class="tab-pane " id="movie1">
	<div class="container">
	<div class="movie-time-wrap">
	
	<div class="datepicker">
	  <span class="datepicker__marker"><i class="fa fa-calendar"></i>Date</span>
	  <input type="text" id="datepicker" value='<?php echo date("d/m/Y",$datenow)?>' class="datepicker__input">
	</div>
	
	<div class="clearfix"></div>
	
	<?php
	   foreach ($datamovie as $dm ) 
	   {
			$datagenreone = $genredb->findOne(array("_id" => new MongoId(trim($dm['genre']))));
	$datacontributorone = $contributordb->findOne(array("_id" => new MongoId(trim($dm['contributor']))));
	
	$dataschedule = $scheduledb->find(array("studio" => trim($studioid),"movie" => trim($dm['_id']),"active" => "1"));	
	$datascheduleone = $scheduledb->findOne(array("studio" => trim($studioid),"movie" => trim($dm['_id']),"active" => "1"));	
	if(strlen($datascheduleone['_id'])>0)
	{
		$image="";
	if (isset($dm['image']))
	{
		if(strlen(trim($dm['image'])) > 0)
	{
		$path_parts = pathinfo($dm['image']);
	$f = $path_parts['filename'];
	$ext = $path_parts['extension'];
	$url = $f.".424x424.".$ext;
	$image= CDN.'image/'.$url;
		}
	}
	?>		
	<div class="col-sm-6">                            
	<!-- Movie variant with time -->
	<div class="movie movie--time">
	<div class="row">
	<div class="col-sm-6 col-md-5">
	    <div class="movie__images">
	        <span class="movie__rating"><?php echo $dm['rating']?></span>
	        
	        <img alt='' src="<?php echo $image?>">
	    </div>
	</div>
	
	<div class="col-sm-6 col-md-7">
	    <a href='/moviedetail/index?movieid=<?php echo $dm['_id']?>' class="movie__title"><?php echo $dm['name']?> </a>
	
	    <p class="movie__time"><?php echo $dm['duration']?> Hours</p>
	
	    <p class="movie__option"><a href="javascript:;"><?php echo $datagenreone['name']?></a> | <a href="javascript:;"><?php echo $datacontributorone['name']?></a> </p>
	</div>
	    <div class="time-select">
	        <ul class="items-wrap">
	            <?php
	            if(isset($dataschedule))
	            {
	            	foreach($dataschedule as $dsc)
	            	{
	            ?>
	            		<li class="time-select__item" data-time='<?php echo $dsc['scheduletime']?>'><?php echo $dsc['scheduletime']?></li>
		                                                               
	            <?php		
	            	}
	            }
	            
	            ?>
	        </ul>
	    </div>
	
	</div>
	</div>
	<!-- Movie variant with time -->
	</div>
	
	<?php	
			}
		}								   
	  ?>
	</div>
	</div>
	</div>
	<div class="tab-pane" id="comment1">
	<div class="container">
	<div class="comment-wrapper">
	<form id="comment-form" class="comment-form" method='post'>
	<textarea class="comment-form__text" placeholder='Add you comment here'></textarea>
	<label class="comment-form__info">250 characters left</label>
	<button type='submit' class="btn btn-md btn--danger comment-form__btn">add comment</button>
	</form>
	
	<div class="comment-sets comment--light">
	
	  <!--<div class="comment">
	      <div class="comment__images">
	          <img alt='' src="http://placehold.it/50x50">
	      </div>
	
	      <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Roberta Inetti</a>
	      <p class="comment__date">today | 03:04</p>
	      <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
	      <a href='#' class="comment__reply">Reply</a>
	  </div>
	
	
	
	  <div class="comment-more">
	      <a href="#" class="watchlist">Show more comments</a>
	  </div>!-->
	
	  </div>
	</div>
	</div>
	</div>
	<div class="tab-pane active" id="map1">
	<div id='cinema-map' class="map"></div>
	</div>
	  </div>
	</div>
	
	</section>
	
	<div class="clearfix"></div>		
	<script type="text/javascript">
	$(document).ready(function() {
	    init_Cinema();
	});
	</script>
