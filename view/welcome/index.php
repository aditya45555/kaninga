<?php
$db = Db::init();
$genredb = $db->genres;
$moviedb = $db->movies;
$contributordb = $db->contributors;
$scheduledb = $db->schedules;
?>

<?php

foreach($js as $j)
{
?>

	<script src="<?php echo $j?>"></script>

<?php	
}

?>

 <!-- Slider -->
<div class="bannercontainer">
    <div class="banner">
        <ul>
        	<?php
        	foreach ($dataslidebanner as $dsb) 
        	{
        		$image="";
				if (isset($dsb['image']))
				{
					if(strlen(trim($dsb['image'])) > 0)
					{
						$path_parts = pathinfo($dsb['image']);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".".$ext;
						$image= CDN.'image/'.$url;
					}
				}
        		
        	?>
                <li data-transition="fade" data-slotamount="7" class="slide" data-slide='<?php echo $dsb['title']?>'>
                    <img class="img-slide" alt='' src="<?php echo $image?>" >
                    <div class="caption slide__name margin-slider" 
                         data-x="right" 
                         data-y="80" 

                         data-splitin="chars"
                         data-elementdelay="0.1"

                         data-speed="700" 
                         data-start="1400" 
                         data-easing="easeOutBack"

                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"

                         data-frames="{ typ :lines;
                                     elementdelay :0.1;
                                     start:1650;
                                     speed:500;
                                     ease:Power3.easeOut;
                                     animation:x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                     },
                                     { typ :lines;
                                     elementdelay :0.1;
                                     start:2150;
                                     speed:500;
                                     ease:Power3.easeOut;
                                     animation:x:0;y:0;z:0;rotationX:00;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                     }
                                     "


                        data-splitout="lines"
                        data-endelementdelay="0.1"
                        data-customout="x:-230;y:0;z:0;rotationX:0;rotationY:0;rotationZ:90;scaleX:0.2;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%"

                        data-endspeed="500"
                        data-end="8400"
                        data-endeasing="Back.easeIn"
                         >
                        <?php echo $dsb['title']?>
                    </div>
                 
                    <div class="caption slide__text margin-slider customin customout" 
                         data-x="right" 
                         data-y="250"
                         data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                         data-speed="400" 
                         data-start="3000"
                         data-endspeed="400"
                         data-end="8000"
                         data-endeasing="Back.easeIn">
                        <?php echo $dsb['description']?>
                     </div>
                    <div class="caption margin-slider skewfromright customout " 
                         data-x="right" 
                         data-y="324"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="400" 
                         data-start="3300"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-end="7700"
                         data-endeasing="Power4.easeOut">
                         <!--a href="<?php //echo $dsb['link']?>" onclick="test('<?php //echo $dsb['link']?>')"  class="btn btn-md btn--danger btn--wide slider--btn">detail film</a-->
                     </div>
                </li>
           <?php
           }
           ?> 
        </ul>
    </div>
 </div>

<!--end slider -->

<section class="container">
<div class="movie-best">
    <!--  <div class="col-sm-10 col-sm-offset-1 movie-best__rating">Selected Movies</div> -->
     <div class="col-sm-12 change--col">
     	
     	<?php
     	foreach($datamovierating as $dmr)
     	{
     		$datacontributorone = $contributordb->findOne(array("_id" => New MongoId(trim($dmr['contributor']))));
			$datagenreone = $genredb->findOne(array("_id" => New MongoId(trim($dmr['genre']))));
			
     		$image="";
			if (isset($dmr['image']))
			{
				if(strlen(trim($dmr['image'])) > 0)
				{
					$path_parts = pathinfo($dmr['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".c380x600.".$ext;
					$image= CDN.'image/'.$url;
				}
			}
     	?>
            <div class="movie-beta__item " >
                <img alt='' src="<?php echo $image?>">
                 <!--span class="best-rate"><?php //echo helper::getRatingMovie($dmr['_id'])?></span-->

                 <ul style="width: 100%; text-align: left;" class="movie-beta__info">
                     <li><span class="best-voted"><?php echo $dmr['name']?></span></li>
                     <li>
                        <p class="movie__time"><?php echo $dmr['duration']." Hours" ?></p>
                        <p><?php echo $datacontributorone['name']?> | <?php echo $datagenreone['name']?>  </p>
                        <p>0 comments</p>
                     </li>
                     <li class="last-block">
                         <a href="/moviedetail/index?movieid=<?php echo $dmr['_id']?>" class="slide__link">Detail</a>
                     </li>
                 </ul>
             </div>
         <?php
		}
         ?>
        
     </div>
    <div class="col-sm-10 col-sm-offset-1 movie-best__check">Now Playing</div>
</div>

<div class="col-sm-12">
    <div class="mega-select-present mega-select-top mega-select--full">
        <div class="mega-select-marker">
            <div class="marker-indecator location">
                <p class="select-marker"><span>Find movies by</span> <br> Location</p>
            </div>

            <div class="marker-indecator cinema">
                <p class="select-marker"><span>Find movies by</span> <br> Theater</p>
            </div>

            <div class="marker-indecator film-category">
                <p class="select-marker"><span>Find movies by</span> <br> Genre</p>
            </div>
        </div>

          <div class="mega-select pull-right">
              <span class="mega-select__point">Find by</span>
              <ul class="mega-select__sort">
                  <li class="filter-wrap"><a href="#" class="mega-select__filter filter--active" data-filter='location'>Location</a></li>
                  <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='cinema'>Theater</a></li>
                  <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='film-category'>Genre</a></li>
              </ul>

              <input name="search-input" type='text' class="select__field">              
              
              <div class="select__btn">
                <a href="#" class="btn btn-md btn--danger location">Find<span class="hidden-exrtasm"> Movie</span></a>
                <a href="#" class="btn btn-md btn--danger cinema">Find<span class="hidden-exrtasm"> Movie</span></a>
                <a href="#" class="btn btn-md btn--danger film-category">Find<span class="hidden-exrtasm"> Movie</span></a>
               </div>

              <div class="select__dropdowns">
                  <ul class="select__group location">
                  	<?php
                  	foreach($datastudio as $st)
                  	{
                  	?>
                  		
                  		<li class="select__variant" data-value='<?php echo $st['subdistrict']?>'><?php echo $st['subdistrict']?></li>
                  	
                  	<?php
                  	}
                  	
                  	?>
                  </ul>

                  <ul class="select__group cinema">
                  	<?php
                  	foreach($datastudio as $st)
                  	{
                  	?>
                  	 	<li class="select__variant" data-value='<?php echo $st['name']?>'><?php echo $st['name']?></li>
                  	<?php
                  	}
                  	?>
                   </ul>

                  <ul class="select__group film-category">
                  	<?php
                  	foreach($datagenre as $ge)
                  	{
                  	?>
                   		<li class="select__variant" data-value="<?php echo $ge['name']?>"><?php echo $ge['name']?></li>	
                  	<?php
					}
                  	?>
                  </ul>
              </div>
          </div>
      </div>
</div>
<div class="clearfix"></div>
<h2 id='target' class="page-heading heading--outcontainer">Now Playing</h2>
<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-8 col-md-9">
        	<?php
        	$limitmovieplaying = 0;
			$n=2;
        	foreach ($data as $d) 
        	{
        		$class = 'movie--test--left';
				if($limitmovieplaying == $n) {
					$class = 'movie--test--right';
					$n += 2;
				}					
        		$scheduleone = $scheduledb->findOne(array("movie" => trim($d['_id']),"active" => "1","scheduledate" => $date+1));
			
				if(isset($scheduleone))
				{
					if($limitmovieplaying<4)
					{
	            		$genre = $genredb->findOne(array('_id'=> new MongoId(trim($d['genre']))));
						$contributor = $contributordb->findOne(array('_id'=> new MongoId(trim($d['contributor']))));
						$image="";
						if (isset($d['image']))
						{
							if(strlen(trim($d['image'])) > 0)
							{
								$path_parts = pathinfo($d['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".c211x211.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	            	?>
	                    <!-- Movie variant with time -->
	                        <div class="movie movie--test movie--test--dark <?php echo $class; ?>">
	                            <div class="movie__images" >
	                                <a href="/moviedetail/index?movieid=<?php echo $d['_id']?>" class="movie-beta__link" >
	                                    <img alt='' src=<?php echo $image?> >
	                                </a>
	                            </div>
	
	                            <div class="movie__info">
	                                <a href='/moviedetail/index?movieid=<?php echo $d['_id']?>' class="movie__title"><?php echo $d['name']?>  </a>
	
	                                <p class="movie__time"><?php echo $d['duration']?></p>
	
	                                 <a href="#"><?php echo $contributor['name'];?></a> | <a href="#"><?php echo $genre['name']?></a></p>
	                                
	                                <!--div class="movie__rate">
	                                	<div class="score" data-score="<?php //echo $d['rating']; ?>" data-id="<?php //echo $d['_id']; ?>"></div>
	                                    <span class="movie__rating"><?php //echo helper::getRatingMovie($d['_id'])?></span>
	                                </div-->               
	                            </div>
	                        </div>
	                     <!-- Movie variant with time -->
	            <?php
	            	$limitmovieplaying++;
	            	}
            	}
            	}
           		 ?>
           	<!--Read More Movie Playing-->
           	<?php
           	if($countdistinctschedule>4)
           	{
           	?>
           		<a href="/movieplaying/index" class="btn read-more post--btn">Others</a>
           	<?php	
           	}
           	?> 
           		  
            <div class="clearfix"></div> 
       		<h2 class="page-heading">Coming Soon</h2>
       		 
       		 	<?php
       		 	$limitcommingsoon = 0;
				$countcommmingsoon = 0;
        	foreach ($data as $d) 
        	{
        		$scheduleone = $scheduledb->findOne(array("movie" => trim($d['_id'])));
				
				if(!isset($scheduleone))
				{
					if($limitcommingsoon<4)
					{
	            		$genre = $genredb->findOne(array('_id'=> new MongoId(trim($d['genre']))));
						$contributor = $contributordb->findOne(array('_id'=> new MongoId(trim($d['contributor']))));
						$image="";
						if (isset($d['image']))
						{
							if(strlen(trim($d['image'])) > 0)
							{
								$path_parts = pathinfo($d['image']);
								$f = $path_parts['filename'];
								$ext = $path_parts['extension'];
								$url = $f.".c211x211.".$ext;
								$image= CDN.'image/'.$url;
							}
						}
	            	?>
	                    <!-- Movie variant with time -->
	                        <div class="movie movie--test movie--test--dark movie--test--left">
	                            <div class="movie__images" >
	                                <a href="/moviedetail/index?movieid=<?php echo $d['_id']?>" class="movie-beta__link" >
	                                    <img alt='' src=<?php echo $image?> >
	                                </a>
	                            </div>
	
	                            <div class="movie__info">
	                                <a href='/moviedetail/index?movieid=<?php echo $d['_id']?>' class="movie__title"><?php echo $d['name']?>  </a>
	
	                                <p class="movie__time"><?php echo $d['duration']?></p>
	
	                                 <a href="#"><?php echo $contributor['name'];?></a> | <a href="#"><?php echo $genre['name']?></a></p>
	                                
	                                <div class="movie__rate">
	                                    <span class="movie__rating"><?php echo helper::getRatingMovie($d['_id']);?></span>
	                                </div>               
	                            </div>
	                        </div>
	                     <!-- Movie variant with time -->
	            <?php
	            $limitcommingsoon++;
	            	}
				$countcommmingsoon++;
            		}
           		 }
            	?>
            	
            	<!--Read More Commingsoon-->
           	<?php
           	if($countcommmingsoon>4)
           	{
           	?>
           		<a href="/commingsoon/index" class="btn read-more post--btn">Others</a>
           	<?php	
           	}
           	?> 	 	
        </div>
      
        <aside class="col-sm-4 col-md-3" style="display: inline-block;margin-top: 0px;">
            <div class="sitebar first-banner--left">             
                <?php
                foreach($datamovienew as $dc)
                {
                	$image="";
					if (isset($dc['image']))
					{
						if(strlen(trim($dc['image'])) > 0)
						{
							$path_parts = pathinfo($dc['image']);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".500x500.".$ext;
							$image= CDN.'image/'.$url;
						}
					}
                ?>
					<div class="banner-wrap first-banner--left">
                    <img alt='banner' src="<?php echo $image?>">
                	</div>
				<?php
					
                }
                ?>  
                
                <!--<div class="promo marginb-sm">
                  <div class="promo__head">A.Movie app</div>
                  <div class="promo__describe">for all smartphones<br> and tablets</div>
                  <div class="promo__content">
                      <ul>
                          <li class="store-variant"><a href="#"><img alt='' src="/public/images/apple-store.svg"></a></li>
                          <li class="store-variant"><a href="#"><img alt='' src="/public/images/google-play.svg"></a></li>
                          <li class="store-variant"><a href="#"><img alt='' src="/public/images/windows-store.svg"></a></li>
                      </ul>
                  </div>
              </div>!-->
              
            </div>
        </aside>
         
    </div>                
</div>
	
<div class="col-sm-12">
    <h2 class="page-heading">Last Schedule</h2>

	<?php
	foreach ($dataschedule as $ds) 
	{
		$movie = $moviedb->findOne(array("_id" => new MongoId(trim($ds['movie'])) ));
		$image="";
			if (isset($movie['image']))
			{
				if(strlen(trim($movie['image'])) > 0)
				{
					$path_parts = pathinfo($movie['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".c270x330.".$ext;
					$image= CDN.'image/'.$url;
				}
			} 
	?>
        <div class="col-sm-4 similar-wrap col--remove" >
            <div class="post post--preview post--preview--wide">
                <div class="post__image">
                    <img alt='' src=<?php echo $image?> > 
                    <!--div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div-->
                </div>
                <p class="post__date"><?php echo date('d M Y',$ds['scheduledate'])." ".$ds['scheduletime'];?> </p>
                <a href="/moviedetail/index?movieid=<?php echo $movie['_id']?>" class="post__title" style="width:100%"><?php echo $movie['name'];?></a>
                <a href="/moviedetail/index?movieid=<?php echo $movie['_id']?>" class="btn read-more post--btn">Detail</a>
            </div>
        </div>
	<?php
	}
	?>
</div>
    
</section>