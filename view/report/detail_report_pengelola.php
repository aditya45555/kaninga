<?php
	$db = Db::init();
	$colstd = $db->studios;
	$coltrx = $db->transactions;
	$colmovie = $db->movies;
?>
<section class="container">
	<div>
		<p style="text-align: right;margin-right: 690px;"><a href="/report/dash_pengelola">BACK</a></p>
		<?php
			$carifilm = $colmovie->findone(array('_id' => new MongoId($idfilm)));
			echo '<h3 align="center">'.$carifilm['name'].'</h3>';
		?>
	<table align="center" style="width: 50%; margin-top: 50px;">
		<tr style="outline: thin solid; height: 50px;" align="center">
			<td>No.</td>
			<td>Schedule</td>
			<td>Jumlah Penonton</td>
			<td>Total Penjualan Tiket</td>
		</tr>
		<?php
			
			if ($jumlahjadwal > 0){
				$data = array();
				$i = 1;	
				foreach ($carijadwal as $cj) {
					$totaltiket = 0;
					$totalpenjualan = 0;
					
					echo '<tr align="center">';
					echo '<td>'.$i.'</td>';
					echo '<td>'.date('d-m-Y', $cj['scheduledate']).'</td>';
					
					$caritrx = $coltrx->find(array('schedule' => trim($cj['_id'])));
					foreach($caritrx as $ctr){
						$totaltiket += $ctr['qty'];
						$totalpenjualan += $ctr['qty'] * $ctr['price'];
					}
					echo '<td>'.$totaltiket.'</td>';
					echo '<td>'.number_format($totalpenjualan, 0,',','.').'</td>';
					echo '</tr>';
				$i++;
				}
			}else{
				echo '<tr><td>Data tidak ditemukan!</td></tr>';
			}
		?>
	</table>
	</div>
</div>