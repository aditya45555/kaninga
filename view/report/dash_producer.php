<?php
	$db = Db::init();
	$colsch = $db->schedules;
	$colstudio = $db->studios;
	
?>
<section class="container">
	<div class="col-xs-8 col-sm-8 col-md-4 body-menu">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<h6 style="font-size: 15px;" class="bold">FILM</h6>
	            <ul>
	            	<?php
	            	foreach($datamovie as $dtnw) {
	            		echo '<li>';
						echo '<div class="col-xs-12 col-sm-12 col-md-12 boxgradasi" style="margin:15px 0 15px 0;">';
	            		if (isset($dtnw['image'])){
	            			if(strlen(trim($dtnw['image'])) > 0)
								{
									$path_parts = pathinfo($dtnw['image']);
									$f = $path_parts['filename'];
									$ext = $path_parts['extension'];
									$url = $f.".c150x80.".$ext;
									$image= CDN.'image/'.$url;
									echo '<a style="padding-left:0;" href="/report/dash_contributor?idmovie='.$dtnw['_id'].'">';
									echo '<div class="col-xs-6 col-sm-6 col-md-6"><img style="margin-left:0px" width="150" height="80" src="'.$image.'" alt=""/> </div>';
									echo '<div class="col-xs-6 col-sm-6 col-md-6" style="padding-left:10px; padding-top:10px;">'.$dtnw['name'].'</div>';
									echo '</a>';
								}
						echo '</div>';
						echo '</li>';
	            		}
	            	}
	            	?>
	            </ul>
		    </div>
		</div>
	<div id="detail-nowplaying" class="col-xs-12 col-sm-12 col-md-8 body-detail">
		<?php if (count($data) > 0) {?>
		<h4>Transaksi Detail </h4>
		<form method="post" action="/report/contributor_detail">	
		<div>
			<?php
				echo '<label style="padding-right: 72px">FILM</label>';
				echo '<select id="idfilm" name="idfilm">';
				echo '<option value="'.$data['_id'].'">'.$data['name'].'</option>';
				echo '</select>';
			?>
		</div><br>
		<div>
			<label style="padding-right: 62px"> Studio </label>
			<select id="studio" name="studio">
				<?php
					foreach ($datastudio as $dts) {
						echo '<option value="'.$dts['id'].'">'.$dts['name'].'</option>';
					}
				?>
			</select>
		</div><br>
		<div>
			<label style="padding-right: 15px"> Bulan Periode </label>
			<select id="bulan" name="bulan">
				<option value="1">Januari</option>
				<option value="2">Febuari</option>
				<option value="3">Maret</option>
				<option value="4">April</option>
				<option value="5">Mei</option>
				<option value="6">Juni</option>
				<option value="7">Juli</option>
				<option value="8">Agustus</option>
				<option value="9">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select>
		</div><br>
		<div>	
			<label style="padding-right: 12px"> Tahun Periode </label>
			<select id="tahun" name="tahun">
				<option value="2015">2015</option>
				<option value="2016">2016</option>
			</select>
		</div><br>
		<div><button type="submit" value="Submit" class="btn btn-default">Submit</button></div>
		</form>
		<br><br>
		<?php } else { ?>
			<h2>Silahkan pilih film disamping.</h2>
		<?php } ?>
	</div>
	
</section>
<script type="text/javascript">
	$(".mouseWheelButtons .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons .next",
        btnPrev: ".mouseWheelButtons .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
	$(".mouseWheelButtons2 .carousel").jCarouselLite({
        btnNext: ".mouseWheelButtons2 .next",
        btnPrev: ".mouseWheelButtons2 .prev",
        visible: 20,
        mouseWheel: true,
        vertical: true,
        circular: false
    });
    
</script>