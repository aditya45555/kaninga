<?php

class util
{
	public static function passwordHash($pwd)
	{
		$salt = 'POSokoklayau123456_';
		$salt = md5($salt.$pwd);
				
		$options = [
			'cost' => 11,
			'salt' => $salt,
		];
		$pwd = password_hash($pwd, PASSWORD_BCRYPT, $options);
		return $pwd;
	}
}
