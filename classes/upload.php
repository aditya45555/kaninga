<?php

class upload
{
	private $ftp;
	private $hostname;
	private $username;
	private $password;
	
	public function __construct()
    {
    	$this->hostname = "103.232.30.62";
		$this->username = "pos";
		$this->password = "d1g1pos";
        $this->init();
    }
	
	private function rubahfolder($folder)
	{
		$dirExists = $this->ftp->ChangeRemoteDir($folder);
		if ($dirExists == true) {
			
		}
		else {
			$success = $this->ftp->CreateRemoteDir($folder);
			if ($success != true) {
			    print $this->ftp->lastErrorText() . "\n";
			    exit;
			}
			$dirExists = $this->ftp->ChangeRemoteDir($folder);
		}
	}
	
	public function delete($namafilesaja)
	{
		$this->rubahfolder('/image');
		
		$fileSize = $this->ftp->GetSizeByName($namafilesaja);
		if ($fileSize > 0)
		{
			$success = $this->ftp->DeleteRemoteFile($namafilesaja); 
			if ($success != true) {
			    print $this->ftp->lastErrorText() . "\n";
			    exit;
			}
		}
	}
	
	public function upload($namafilepath, $namafilesaja, $ext)
	{		
		$this->rubahfolder('/image');
		
		$localFilename = $namafilepath;
		$remoteFilename = $namafilesaja;
		
		$success = $this->ftp->PutFile($localFilename, $remoteFilename);
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
	}
	
	public function checkFtpDir($dirname) {
		$dirExists = $this->ftp->ChangeRemoteDir($dirname);
		if ($dirExists == true) {
			return true;
		}
		else {
			$success = $this->ftp->CreateRemoteDir($dirname);
			if ($success != true) {
				return false;
			    print $this->ftp->lastErrorText() . "\n";
			    exit;
			}
			$dirExists = $this->ftp->ChangeRemoteDir($dirname);
			return true;
		}
	}
	
	public function download($namafile, $namafilepath)
	{
		//$this->rubahfolder('/home/digibeat');
		$this->rubahfolder('/image');
		
		$localfolder = '/home/digibeat/download/';
		
		$this->ftp->put_RestartNext(true);
		
		$localFilename = $localfolder.$namafile;
		$remoteFilename = $namafilepath;
		
		//  Resume downloading...
		$success = $this->ftp->GetFile($remoteFilename, $localFilename);
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}		
	}	
	
	public function getFileSize($filename, $folder)
	{
		//$this->rubahfolder('/home/digibeat');
		$this->rubahfolder($folder);
		
		$fileSize = $this->ftp->GetSizeByName($filename);
		
		return $fileSize;
	}
	
	private function init()
	{
		$this->ftp = new CkFtp2();

		//  Any string unlocks the component for the 1st 30-days.
		$success = $this->ftp->UnlockComponent('WILLAWFTP_vLuYpQZk9JnJ');
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
		
		$this->ftp->put_Hostname($this->hostname);
		$this->ftp->put_Username($this->username);
		$this->ftp->put_Password($this->password);
		
		//  Connect and login to the FTP server.
		$success = $this->ftp->Connect();
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
	}
	
	function __destruct() {
       $this->ftp->Disconnect();
   	}
}
