<?php

class api
{
	public function __construct()
	{
		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
		header('Content-Type: application/json');
	}

	public function logapis($from,$description, $result='')
	{
		$db = Db::init();
		$logapi = $db->logapis;
		//log api
		$datainsertapi= array(
		'from' => $from,
		'parameter' => $description,
		'result' => $result,
		'time_created' => time()
		);
		$logapi->insert($datainsertapi);	
	}

}
