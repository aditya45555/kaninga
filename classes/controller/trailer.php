<?php
class trailer_controller extends controller
{
	public function index()
	{
		$db=Db::init();
		$moviedb = $db->movies;
		
		$page = $this->getPage();
        $limit = 5;
        $skip = (int)($limit * ($page - 1));
		$countpagging = 0 ;
		
		$datamovie = $moviedb->find()->sort(array("time_created" => -1))->limit($limit)->skip($skip);
		$countpagging = $moviedb->count();
		
		//destroy session search
		if(!isset($_GET['optionsearch']))
		{
			//destroysession
			unset($_SESSION['optionsearch']);
			unset($_SESSION['search']);
		}
		
		if(!empty($_POST))
		{
			$search = null;
			$optionsearch = null;
			
			if(isset($_POST['search']))
			{
				$search = $_POST['search'];
			}
			
			if(isset($_POST['optionsearch']))
			{
				$optionsearch = $_POST['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datamovie = $moviedb->find(array("name" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging = $moviedb->count(array("name" => $regex));
				$_SESSION['optionsearch'] = "1";
				$_SESSION['search'] = $search;
				
			}
		}
		
		if(!empty($_GET))
		{	
			$search = null;
			$optionsearch = null;
			
			if(isset($_GET['search']))
			{
				$search = $_GET['search'];
			}
			
			if(isset($_GET['optionsearch']))
			{
				$optionsearch = $_GET['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datamovie = $moviedb->find(array("name" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging= $moviedb->count(array("name" => $regex));
			}
		}
		
		
		$pg = new Pagination();
		if(!empty($_SESSION['optionsearch']))
		{
			if($_SESSION['optionsearch'] == "1")
			{
				$pg_url = "/trailer/index?page=";
				$pg_url = "/trailer/index?search=".$_SESSION['search']."&&optionsearch=1&&page=";
			}
			else
			{
				$pg_url = "/trailer/index?search=".$_SESSION['search']."&&optionsearch=2&&page=";
			}
		}
		else
		{
			$pg_url = "/trailer/index?page=";	
		}
		
		$pg->pag_url = $pg_url;
		$pg->calculate_pages($countpagging, $limit, $page);
		
		$js[] = "/public/js/external/jquery.magnific-popup.min.js";
		
		$css[] = "/public/css/external/magnific-popup.css";
		
		$var=array(
			"js" => $js,
			"css" => $css,
			"datamovie" => $datamovie,
			"page" => $page,
			"limit" => $limit,
			"skip" => $skip+1,
			"pagination" => $pg->Show()
		);
		$this->render("trailer", "trailer/index.php", $var);
	}
}
?>