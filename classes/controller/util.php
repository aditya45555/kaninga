<?php

class util_controller {
	public function getnowplaying() {
		$id = $_GET['id'];
		$db = Db::init();
		$m = $db->movies;
		$col = $m->findone(array('_id' => new MongoId($id)));	
		
		$var = array('data' => $col);
		echo $this->getView(DOCVIEW.'layout/nowplaying.php', $var);
	}
	
	public function getCity() {
		$id = $_GET['id'];
		$db = Db::init();
		$t = $db->place_towns;
		$studio = $db->studios;
		$mstd = $studio->find(array('towns' => $id))->sort(array('subdistrict' => 1));
		
		$tt = $t->findone(array('_id' => new MongoId($id)));
		$search = array('Kota', 'Kabupaten');
		$tn = str_replace($search, '', $tt['name']);
		
		$var = array('data' => $mstd);
		$list = $this->getView(DOCVIEW.'layout/citydetail.php', $var);
		
		$data = array('name' => trim($tn), 'data' => $list);
		echo json_encode($data);
	}
	
	public function insertcomment() {
		if(!empty($_POST)) {
			$id = '';
			if(isset($_POST['id']))
				$id = trim($_POST['id']);
			$comment = '';
			if(isset($_POST['comment']))
				$comment = trim($_POST['comment']);
			
			if(strlen($id) > 0 && strlen($comment) > 0) {
				$data = array(
					'movie' => $id,
					'comment' => $comment,
					'userid' => $_SESSION['userid'],
					'time_created' => time()
				);
				
				$db = Db::init();
				$commentdb = $db->commentmovies;
				$commentdb->insert($data);
				
				$count = $commentdb->count(array('movie' => $id));								
				
				$dataview = $this->getView(DOCVIEW.'comment/add.php', $data);
				
				$h = array(
					'count' => $count,
					'data' => $dataview
				);
				echo json_encode($h);
				exit;
			}
			
			echo 'gagal';
			exit;			
		}
		
		echo 'gagal';
		exit;
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
	    (include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}
}
