<?php
	class trailer1_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colmovie = $db->movies;
			
			$datamovienw = $colmovie->find(array('commingsoon' => 'not'))->sort(array('time_created' => 1));
			$datamoviecs = $colmovie->find(array('commingsoon' => 'yes'))->sort(array('time_created' => 1));
			
			$var = array(
				'datamovienw' => $datamovienw,
				'datamoviecs' => $datamoviecs
			);
			$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
			$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
			//$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';
			
			$this->render("trailer", "trailer/index1.php", $var);
		}
	}
?>