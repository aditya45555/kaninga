<?php
class commingsoon_controller extends controller
{
	public function index() {
		$db = Db::init();
		$studio = $db->studios;
		$m = $db->movies;
		$t = $db->place_towns;
		
		$datamovie = $m->find(array('commingsoon' => 'yes'));
		$dstudio = $studio->find();
		$dtt = $t->find()->sort(array('name' => 1));
		$datakota = array();
		$search = array('Kota', 'Kabupaten');
		foreach($dstudio as $dst) {
			if(count($datakota) > 0) {
				$n=0;
				$nn=0;
				foreach($datakota as $ddst) {
					if($ddst['id'] == $dst['towns'])
						$n++;
					$nn++;
				}
				if($n == 0) {
					$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
					$tn = str_replace($search, '', $tt['name']);
					$d = array('id' => $dst['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			else {
				$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
				$tn = str_replace($search, '', $tt['name']);
				$d = array('id' => $dst['towns'], 'name' => trim($tn));
				$datakota[] = $d;
			}
		}		
		usort($datakota, function ($a, $b) {
		    return strcmp($a['name'], $b['name']); 
		});
	}
	
	/*public function index()
	{
		
		$db = Db::init();
		$moviedb = $db->movies;
		$commentmoviedb = $db->commentmovies;
		$scheduledb = $db->schedules;
		
		$page = $this->getPage();
        $limit = 5;
        $skip = (int)($limit * ($page - 1));
		$countpagging = 0 ;
		
		$datamovie = $moviedb->find()->sort(array("time_created" => -1))->limit($limit)->skip($skip);
		$datamoviepagging = $moviedb->find();
		
		//destroy session search
		if(!isset($_GET['optionsearch']))
		{
			//destroysession
			unset($_SESSION['optionsearch']);
			unset($_SESSION['search']);
		}
		
		if(!empty($_POST))
		{
			$search = null;
			$optionsearch = null;
			
			if(isset($_POST['search']))
			{
				$search = $_POST['search'];
			}
			
			if(isset($_POST['optionsearch']))
			{
				$optionsearch = $_POST['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datamovie = $moviedb->find(array("name" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$datamoviepagging = $moviedb->find(array("name" => $regex));
				$_SESSION['optionsearch'] = "1";
				$_SESSION['search'] = $search;
				
			}
		}
		
		if(!empty($_GET))
		{	
			$search = null;
			$optionsearch = null;
			
			if(isset($_GET['search']))
			{
				$search = $_GET['search'];
			}
			
			if(isset($_GET['optionsearch']))
			{
				$optionsearch = $_GET['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datamovie = $moviedb->find(array("name" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$datamoviepagging = $moviedb->find(array("name" => $regex));
			}
		}
		
		
		foreach($datamoviepagging as $dm)
		{
			$datascheduleone =  $scheduledb->findOne(array("movie" => trim($dm['_id'])));
			if(!isset($datascheduleone))
			{
				$countpagging++;
			}
		}
		
		$pg = new Pagination();
		if(!empty($_SESSION['optionsearch']))
		{
			if($_SESSION['optionsearch'] == "1")
			{
				$pg_url = "/commingsoon/index?page=";
				$pg_url = "/commingsoon/index?search=".$_SESSION['search']."&&optionsearch=1&&page=";
			}
			else
			{
				$pg_url = "/commingsoon/index?search=".$_SESSION['search']."&&optionsearch=2&&page=";
			}
		}
		else
		{
			$pg_url = "/commingsoon/index?page=";	
		}
		
		$pg->pag_url = $pg_url;
		$pg->calculate_pages($countpagging, $limit, $page);
		
		$var =  array(
		"datamovie" => $datamovie,
		"page" => $page,
		"limit" => $limit,
		"skip" => $skip+1,
		"pagination" => $pg->Show()
		);
		
		$this->js[]= "/public/js/controller/postrating.js";
		$this->render("commingsoon", "commingsoon/index.php", $var);
	}*/
}
?>