<?php
class news_controller extends controller
{
	public function index()
	{
		$db=Db::init();
		$newsdb = $db->news;
		
		$page = $this->getPage();
        $limit = 5;
        $skip = (int)($limit * ($page - 1));
		$countpagging = 0 ;
		
		$datanews = $newsdb->find()->sort(array("time_created" => -1))->limit($limit)->skip($skip);
		$countpagging = $newsdb->count();
		
		//destroy session search
		if(!isset($_GET['optionsearch']))
		{
			//destroysession
			unset($_SESSION['optionsearch']);
			unset($_SESSION['search']);
		}
		
		if(!empty($_POST))
		{
			$search = null;
			$optionsearch = null;
			
			if(isset($_POST['search']))
			{
				$search = $_POST['search'];
			}
			
			if(isset($_POST['optionsearch']))
			{
				$optionsearch = $_POST['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datanews = $newsdb->find(array("title" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging = $newsdb->count(array("title" => $regex));
				$_SESSION['optionsearch'] = "1";
				$_SESSION['search'] = $search;	
			}
			
			else if($optionsearch == "2")
			{
				$regex = new MongoRegex("/^$search/");	
				$datanews = $newsdb->find(array("tag" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging = $newsdb->count(array("tag" => $regex));
				$_SESSION['optionsearch'] = "2";
				$_SESSION['search'] = $search;	
			}
		}
		
		if(!empty($_GET))
		{	
			$search = null;
			$optionsearch = null;
			
			if(isset($_GET['search']))
			{
				$search = $_GET['search'];
			}
			
			if(isset($_GET['optionsearch']))
			{
				$optionsearch = $_GET['optionsearch'];
			}
			
			if($optionsearch == "1")
			{
				$regex = new MongoRegex("/^$search/");	
				$datanews = $newsdb->find(array("title" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging = $newsdb->count(array("title" => $regex));
			}
			
			else if($optionsearch == "2")
			{
				$regex = new MongoRegex("/^$search/");	
				$datanews = $newsdb->find(array("tag" => $regex))->sort(array("time_created" => -1))->limit($limit)->skip($skip);
				$countpagging = $newsdb->count(array("tag" => $regex));
			}
			
		}
		
		$pg = new Pagination();
		if(!empty($_SESSION['optionsearch']))
		{
			if($_SESSION['optionsearch'] == "1")
			{
				$pg_url = "/news/index?page=";
				$pg_url = "/news/index?search=".$_SESSION['search']."&&optionsearch=1&&page=";
			}
			else
			{
				$pg_url = "/news/index?search=".$_SESSION['search']."&&optionsearch=2&&page=";
			}
		}
		else
		{
			$pg_url = "/news/index?page=";	
		}
		$pg->pag_url = $pg_url;
		$pg->calculate_pages($countpagging, $limit, $page);
		
		$var=array(
			"datanews" => $datanews,
			"page" => $page,
			"limit" => $limit,
			"skip" => $skip+1,
			"pagination" => $pg->Show()
		);
		$this->render("news", "news/index.php", $var);
	}
}


?>