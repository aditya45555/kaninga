<?php
class moviedetail_controller extends controller
{
	public function index()
	{
		if(isset($_GET['movieid']))
		{
			$db=Db::init();	
			$moviedb = $db->movies;
			$scheduledb = $db->schedules;
			$genredb = $db->genres;
			$contributordb = $db->contributors;
			$commentdb = $db->commentmovies;
			$galerimoviedb = $db->galerimovies;
			$studiodb = $db->studios;
			
			if(isset($_GET['date']))
			{
				if(strlen($_GET['date'])>0)
				{
					$date = date("Y-m-d",strtotime($_GET['date']));	
				}
				else
				{
					$date = date("Y-m-d",time());
				}
				
				$studioid = $_GET['studioid'];
				
				if(strtoupper($studioid)!="ALL" && strlen($studioid)>0)
				{
					$datastudiofilter = $studiodb->find(array("_id" => new MongoId(trim($studioid))));
				}
				else
				{
					$datastudiofilter = $studiodb->find();
				}
			}
			else
			{
				$date = date("Y-m-d",time());
				$datastudiofilter = $studiodb->find();
				$studioid = null;
			}
			
			
			$movieid = $_GET['movieid'];
			$datenow=strtotime($date);
			
			$datamovie = $moviedb->findOne(array("_id" => new MongoId(trim($movieid))));
			$datagenre = $genredb->findOne(array("_id" => new MongoId(trim($datamovie['genre']))));
			$datacontributor = $contributordb->findOne(array("_id" => new MongoId(trim($datamovie['contributor']))));
			$datacommentdb = $commentdb->find(array("movie" => trim($movieid)))->sort(array('time_created' => -1));
			$datagalerimovieimage = $galerimoviedb->find(array("movie" => trim($movieid),"description" => "Images"));
			$datagalerimovievideo = $galerimoviedb->find(array("movie" => trim($movieid),"description" => "Videos"));
			$datagalerimovieall = $galerimoviedb->find(array("movie" => trim($movieid)));
			$datastudio = $studiodb->find();
			
			
			$countcomment = $commentdb->count(array("movie" => trim($movieid)));
			$countgalerimovieimage = $galerimoviedb->count(array("movie" => trim($movieid),"description" => "Images"));
			$countgalerimovievideo = $galerimoviedb->count(array("movie" => trim($movieid),"description" => "Videos"));
			
			$font[] = "http://fonts.googleapis.com/css?family=Roboto:400,700";
			
			$css[] = "/public/css/external/idangerous.swiper.css"; 
			$css[] = "/public/css/external/magnific-popup.css";
			$css[]=  "/public/css/leaflet.css";
			
			
			$js[] = "/public/js/external/idangerous.swiper.min.js";
			$js[] = "/public/js/external/jquery.magnific-popup.min.js";
			$js[] = "/public/js/maplocation.js";
			$js[] = "https://maps.google.com/maps/api/js?sensor=true";
			$js[] = "/public/js/external/infobox.js";
			$js[] = "/public/js/leaflet.js";			
			$js[] = "/public/js/getdatafiltermoviedetail.js";
			$this->js[]= "/public/js/controller/postrating.js";
			$this->js[]= "/public/js/controller/comment.js";
			$var =  array(
				"movieid" => $movieid,
				"studioid" => $studioid,
				"datenow" => $datenow,
				"datastudiofilter" => $datastudiofilter,
				"datamovie" => $datamovie,
				"datagenre" => $datagenre,
				"datacontributor" => $datacontributor,
				"datagalerimovieimage" => $datagalerimovieimage,
				"datagalerimovievideo" => $datagalerimovievideo,
				"datagalerimovieall" => $datagalerimovieall,
				"datastudio" => $datastudio,
				"datacomment" => $datacommentdb,
				"countcomment" => $countcomment,
				"countgalerimovieimage" => $countgalerimovieimage,
				"countgalerimovievideo" => $countgalerimovievideo,
				"countcomment" => $countcomment,
				"css" => $css,
				"font" => $font,
				"js" => $js
				
			);
			$this->render("moviedetail", "moviedetail/index.php", $var);
		}
		else
		{
			$this->redirect("/notfound/index");
		}	
	}
	
	public function getlatlong()
	{
		if(isset($_GET['movieid']))
		{
		$db = Db::init();
		$movieid = $_GET['movieid'];
		$studiodb = $db->studios;
		$scheduledb = $db->schedules;
		
		if(isset($_GET['date']))
		{
			if(strlen($_GET['date'])>0)
			{
				$date = date("Y-m-d",strtotime($_GET['date']));	
			}
			else
			{
				$date = date("Y-m-d",time());
			}
			
			$studioid = $_GET['studioid'];
			
			if(strtoupper($studioid)!="ALL" && strlen($studioid)>0)
			{
				$datastudiofilter = $studiodb->find(array("_id" => new MongoId(trim($studioid))));
			}
			else
			{
				$datastudiofilter = $studiodb->find();
			}
		}
		else
		{
			$date = date("Y-m-d",time());
			$datastudiofilter = $studiodb->find();
			$studioid = null;
		}
		
		$datenow = strtotime($date);	
		$arr=array();
		
		foreach ($datastudiofilter as $dat ) 
		{
			$datascheduleone = $scheduledb->findOne(array("movie" => trim($movieid),"studio" => trim($dat['_id']),"active" => "1","scheduledate" => $datenow+1));			
			
			if(isset($datascheduleone['_id']))
			{
				if(isset($dat['latitude']))
				{
					if(strlen($dat['latitude'])>0)
					{
						$description="<h2>".$dat['name']."</h2>".
						"<p>".$dat['address']."</p>";	
						$p=array(
							"latitude"=>$dat['latitude'],
							"longitude"=>$dat['longtitude'],
							"description"=>$description
						);
						$arr[]=$p;
					}
				
				}		
			}		
		}
		echo json_encode($arr);
		}
	}
}
?>