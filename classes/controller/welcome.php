<?php

class welcome_controller extends  controller
{
	public function index()
	{
		$db = Db::init();
		$movie = $db->movies;
		$schedule = $db->schedules;
		$genre = $db->genres;
		$slidebanner = $db->slidebanners;
		$studio = $db->studios;
		$date = strtotime(date("Y-m-d",time()));
		$datamovienew = $movie->find()->limit(3)->sort(array('time_created'=>-1));
		$datadistinctischedule = $schedule->distinct("movie",array("active"=>"1","scheduledate" => $date+1));
		$countdatadistinctschedule = count($datadistinctischedule);
		$data = $movie->find()->sort(array('time_created'=>-1));
		$dataschedule = $schedule->find()->limit(3)->sort(array('time_created'=>-1));
		$dataslidebanner = $slidebanner->find()->limit(3)->sort(array('time_created'=>-1));
		$datastudio = $studio->find();
		$datagenre = $genre->find();
		$datamovierating = $movie->find()->sort(array('time_created' => -1))->limit(6);
		$count = $data->count();
		
		
		$js[] = "/public/js/redirectchekout.js"; 
			
		$var = array(
			'data' => $data,
			'datamovienew' => $datamovienew,
			'dataschedule' => $dataschedule,
			'dataslidebanner' => $dataslidebanner,
			'datastudio' => $datastudio,
			'datagenre' => $datagenre,
			'date' => $date,
			'datamovierating' => $datamovierating,
			'js' => $js,
			'countdistinctschedule' => $countdatadistinctschedule
		);	
		$this->render("home", "welcome/index2.php", $var);
	}
	
	public function info()
	{
		echo phpinfo();
	}
	
	public function test()
	{
		echo "test";
	}
}
