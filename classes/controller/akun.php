<?php
	class akun_controller {
			
		public function login() {
		$error = array();
		$email = '';
		$pass = '';
		$action = '/login';
			
		if(!empty($_POST)) {
			$email = trim($_POST['email']);
			$pass = trim($_POST['pass']);
			
			$db = Db::init();
			$users = $db->users;
			$where = array('email' => $email);
			$col = $users->findOne($where);
			
			if(! isset($col['_id']))
				$error[] = array('error' => 'email doesnt exists');			
			else {				
				$oldpass = '';
				if(isset($col['password']))
					$oldpass = $col['password'];
				
				$u = new user();
				if($u->checkPassword($pass, $oldpass)) {
					$datal = array(
						'last_login' => time()
					);
					$wherel = array('_id' => new MongoId(trim($col['_id'])));
					$users->update($wherel,array('$set' => $datal));
					
					$_SESSION['userid'] = trim($col['_id']);
					$_SESSION['user'] = trim($col['name']);
					$_SESSION['email'] = $email;
					$_SESSION['client'] = $col['clientid'];
					$_SESSION['tipe'] = $col['tipe'];
					$_SESSION['contributor'] = $col['contributor'];
					
					
					
					if ($col['tipe'] == "Contributor"){
						echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_contributor"></html>';
					}
					else if ($col['tipe'] == "Client"){
						echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_pengelola"></html>';
					}
					else {
						echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_event"></html>';
					}
					exit;
				}
				
				$error[] = array('error' => 'wrong password');				
			}			
		}
		$this->css[] = "/public/css/login.css";
		include(DOCVIEW."template/login.php");
	}	
	
	public function logout()
	{
		session_destroy ();
		
		header( 'Location: /welcome/index' ) ;
		exit();
	}
	
	public function kereport(){
		if (isset($_SESSION['user'])){
			if ($_SESSION['tipe'] == "Contributor"){
				echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_contributor"></html>';
			}
			else if ($_SESSION['tipe'] == "Client"){
				echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_pengelola"></html>';
			}
			else {
				echo '<html><meta http-equiv="refresh" content="0; url=/report/dash_event"></html>';
			}
		}
	}
	
		
	}
?>