<?php

class booking_controller extends controller {
	
	public function index() {
		$token = helper::checkOutBooking();
		$db = Db::init();
		$movie = $db->movies;
		$studio = $db->studios;
		$datamovie = $movie->find()->sort(array('time_created'=>-1));
		$datastudio = $studio->find(array('clientid' => array('$ne' => '56bc9c841d85e9929f8b458b')));

		$var = array(
			'token' => $token,
			'movie' => $datamovie,
			'studio' => $datastudio
		);
		
		$this->css[] = '/public/css/gozha-nav.css';
		$this->css[] = '/public/css/external/idangerous.swiper.css';
		
		$this->js[] = '/public/js/external/idangerous.swiper.min.js';
		$this->js[] = '/public/js/controller/booking.js';
				
		$this->render('booking', 'booking/index.php', $var);
	}
	
	public function status() {
		print_r($_GET);
	}
	
	public function transfer() {
		
	}	
	
	public function notif() {
		print_r($_GET);
	}
}
