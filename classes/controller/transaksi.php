<?php
	class transaksi_controller extends controller{
		
		public function index(){
		$page = $this->getPage();
		
        $limit = 10;
        $skip = (int)($limit * ($page - 1));
		
		$tglstart = date('d-m-Y', time());
		if(isset($_SESSION['transaction_start']))
			$tglstart = $_SESSION['transaction_start'];
		$tglend = date('d-m-Y', time());
		if(isset($_SESSION['transaction_end']))
			$tglend = $_SESSION['transaction_end'];
		
		if(!empty($_POST)) {
			$tglstart = trim($_POST['start']);
			$tglend = trim($_POST['end']);
			
			$_SESSION['transaction_start'] = $tglstart;
			$_SESSION['transaction_end'] = $tglend;
		}
		
		$dtgl = explode('-', $tglstart);
		$start = strtotime($dtgl[2].'-'.$dtgl[1].'-'.$dtgl[0].' 00:00:00');
		$dtgl = explode('-', $tglend);
		$end = strtotime($dtgl[2].'-'.$dtgl[1].'-'.$dtgl[0].' 00:00:00');
		
		$end = strtotime('+1 day', $end);
		
		$query = array('transactiondate' => array('$gt' => $start, '$lt' => $end));
		if($_SESSION['tipe']=="Client")
			$query['clientid'] = new MongoId($_SESSION['clientid']);
		
		$db = Db::init();
		$transaction = $db->transactions;
		
		$data = $transaction->find($query)->skip($skip)->limit($limit)->sort(array('transactiondate'=>-1));
		
		$count = $transaction->count($query);

		$pg = new Pagination();
        $pg->pag_url = '/transaksi/index?page=';
        $pg->calculate_pages($count, $limit, $page);
		
		$var = array(
			'start' => $tglstart,
			'end' => $tglend,
			'limit' => $limit,
			'data' => $data,
			'skip' => $skip+1,
			'page' => $page,
			'clientid' => $_SESSION['clientid'],
			'link' => '/transaksi/index',
			'pagination' => $pg->Show(),
		);
		$this->render("", "transaksi/index.php", $var);
	}

		public function indexday()
	{
		$page = $this->getPage();
		$limit = 10;
        $skip = (int)($limit * ($page - 1));
		
		$studioid = '';
		if(isset($_SESSION['transaction_day_studio']))
			$studioid = $_SESSION['transaction_day_studio'];
		$date = date('d-m-Y', time());
		if(isset($_SESSION['transaction_day']))
			$date = $_SESSION['transaction_day'];
			
		if(!empty($_POST)) {
			$studioid = trim($_POST['studio']);
			$date = trim($_POST['date']);
			
			$_SESSION['transaction_day_studio'] = $studioid;
			$_SESSION['transaction_day'] = $date;
		}				
		
		$db=Db::init();
		$movie = $db->movies;
		$transaction = $db->transactions;
		
		$dtgl = explode('-', $date);
		$start = strtotime($dtgl[2].'-'.$dtgl[1].'-'.$dtgl[0].' 00:00:00');
		$end = strtotime('+1 day', $start);
		
		$query = array('transactiondate' => array('$gt' => $start, '$lt' => $end));
		if($_SESSION['tipe']=="Client")
			$query['clientid'] = new MongoId($_SESSION['clientid']);
		if(strlen(trim($studioid)) > 0) {
			$query = array(
				'transactiondate' => array('$gt' => $start, '$lt' => $end),
				'studio' => trim($studioid)
			);
		}
		
		$data = $transaction->find($query)->limit($limit)->skip($skip)->sort(array('transactiondate' => -1));
		$count = $transaction->count($query);
		
		$temp = $transaction->find($query);
		$total = 0;
		$qty = 0;
		foreach($temp as $dtmp) {
			$moviedata = $movie->findone(array('_id' => new MongoId(trim($dtmp['movie']))));
			$v = 0;
			foreach($dtmp['voucher'] as $vv)
				$v = intval($vv['nilai']);
			
			$price = intval($dtmp['price']-($dtmp['price']*($dtmp['diskon']/100)));
			$tprice = intval($price-($price*($moviedata['tax']/100)));
			$total += intval(($tprice*$dtmp['qty'])-$v);
			$qty += intval($dtmp['qty']);
		}
		
		$pg = new Pagination();
       	$pg->pag_url = '/transaksi/indexday?page=';
        $pg->calculate_pages($count, $limit, $page);		
		$var = array(
			'studioid' => $studioid,
			'date'=> $date,
			'limit' => $limit,
			'data' => $data,
			'page' => $page,
			'link' => '/transaksi/indexday',
			'pagination' => $pg->Show(),
			'totalall' => $total,
			'totalsoldticket' => $qty			
		);
		$this->render("", "transaksi/indexday.php", $var);						
	}
		
		public function indexmonth()
	{
		$studioid = '';
		if(isset($_SESSION['transaction_month_studio']))
			$studioid = $_SESSION['transaction_month_studio'];
		$month = date('n', time());
		if(isset($_SESSION['transaction_month']))
			$month = $_SESSION['transaction_month'];
		$year = date('Y', time());
		if(isset($_SESSION['transaction_month_year']))
			$year = $_SESSION['transaction_month_year'];
			
		if(!empty($_POST)) {
			$studioid = trim($_POST['studio']);
			$month = trim($_POST['month']);
			$year = trim($_POST['year']);
			
			$_SESSION['transaction_month_studio'] = $studioid;
			$_SESSION['transaction_month'] = $month;
			$_SESSION['transaction_month_year'] = $year;
		}
		
		$db = Db::init();
		$transaction = $db->transactions;
		$studio=$db->studios;
		
		$client=$db->clients;
		$movie = $db->movies;
		
		$bb = $month;
		if(strlen(trim($month)) == 1)
			$bb = '0'.$month;
		
		$start = strtotime($year.'-'.$bb.'-01 00:00:00');
		$end = strtotime('+1 month', $start);
		
		$query = array('transactiondate' => array('$gt' => $start, '$lt' => $end));
		if(strlen(trim($studioid)) > 0)
			$query['studio'] = $studioid;
		if($_SESSION['tipe']=="Client")
			$query['clientid'] = new MongoId($_SESSION['clientid']);
		
		$temp = $transaction->find($query)->sort(array('transactiondate' => 1));
		
		
		$data = array();		
		foreach($temp as $dtmp) {
			$moviedata = $movie->findone(array('_id' => new MongoId(trim($dtmp['movie']))));
			if(count($data) > 0) {
				$n=0;
				$nn=0;
				foreach($data as $dat) {
					if(intval($dat['date']) == intval(date('j', $dtmp['transactiondate']))) {
						$v = 0;
						foreach($dtmp['voucher'] as $vv)
							$v = intval($vv['nilai']);
						
						$price = intval($dtmp['price']-($dtmp['price']*($dtmp['diskon']/100)));
						$tprice = intval($price-($price*($moviedata['tax']/100)));
						$total = ($tprice*$dtmp['qty'])-$v;
						
						$data[$n]['ticket'] += $dtmp['qty'];
						$data[$n]['total']+= $total;
						
						$nn++;
					}
					$n++;
				}
				if($nn == 0) {
					$v = 0;
					foreach($dtmp['voucher'] as $vv)
						$v = intval($vv['nilai']);
					
					$price = intval($dtmp['price']-($dtmp['price']*($dtmp['diskon']/100)));
					$tprice = intval($price-($price*($moviedata['tax']/100)));
					$total = ($tprice*$dtmp['qty'])-$v;
									
					$d = array(
						'date' => intval(date('j', $dtmp['transactiondate'])),
						'ticket' => intval($dtmp['qty']),
						'total' => $total
					);
					
					$data[] = $d;
				}
			}
			else {
				$v = 0;
				foreach($dtmp['voucher'] as $vv)
					$v = intval($vv['nilai']);
				
				$price = intval($dtmp['price']-($dtmp['price']*($dtmp['diskon']/100)));
				$tprice = intval($price-($price*($moviedata['tax']/100)));
				$total = ($tprice*$dtmp['qty'])-$v;
								
				$d = array(
					'date' => intval(date('j', $dtmp['transactiondate'])),
					'ticket' => intval($dtmp['qty']),
					'total' => $total
				);
				
				$data[] = $d;
			}
		}
		
		
		
		$mtotal = 0;
		$tckttotal = 0;
		foreach($data as $dat) {
			$mtotal += intval($dat['total']);
			$tckttotal += intval($dat['ticket']);
		}
		
		$var = array(
			'totalall' => $mtotal,
			'totalsoldticket' => $tckttotal,
			'studioid' => $studioid,
			'month' => $month,
			'year' => $year,
			'start' => $start,
			'datatransaksi' => $data,
			'clientid' => $_SESSION['clientid'],
			'link' => '/transaksi/indexmonth',
		);
		$this->render("", "transaksi/indexmonth.php", $var);
	}
		
		public function detail_movie(){
			if($_SESSION['tipe']=="Contributor"){
			$movie = "";
			if(isset($_GET['movie'])){
				$movie = $_GET['movie'];
			}
			$db = Db::init();
			$studiodb = $db->studios;
			$moviedb = $db->movies;
			$datamovieone = $moviedb->findOne(array("_id" => new MongoId(trim($movie))));
			$page = $this->getPage();
	        $limit = 10;
	        $skip = (int)($limit * ($page - 1));
			$datastudio = $studiodb->find()->limit($limit)->skip($skip);
			
			$count = $studiodb->count();
			$pg = new Pagination();
		    $pg->pag_url = "/transaksi/detail_movie?movie=$movie&&page=";
		    $pg->calculate_pages($count, $limit, $page);
			$title = "Transaksi Detail Movie ".$datamovieone['name'];
			$var = array(
				'limit' => $limit,
				'datastudio' => $datastudio,
				'skip' => $skip+1,
				'movie' => $movie,
				'page' => $page,
				'title' => $title,
				'pagination' => $pg->Show(),
			);
			$this->render("","transaksi/detail_movie.php", $var);
		}
		else{
			$this->redirect("/welcome/index");
		}
		}
	}

?>