<?php
	class report_controller extends controller{
			
		
		public function dash_event(){
			$idmovie = isset($_GET['idmovie']) ? $_GET['idmovie'] : '';
				
			$db = Db::init();
			$colsch = $db->schedules;
			$colstudio = $db->studios;
			$colmovie = $db->movies;
			
			$datamovie = $colmovie->find();
			$data = array();
			if(strlen(trim($idmovie)) > 0)
				$data = $colmovie->findone(array('_id' => new MongoId($idmovie)));
			
			$dataschedule = $colsch->find(array('movie' => $idmovie))->sort(array('studio' => 1));
				$datastudio = array();
				foreach ($dataschedule as $key) {
					if (count($datastudio) > 0){
						$i = 0;
						$ii = 0;
						foreach ($datastudio as $ddst) {
							if($ddst['id'] == $key['studio'])
								$i++;
							$ii++;
						}
						if ($i == 0) {
							$td = $colstudio->findOne(array('_id' => new MongoId($key['studio'])));
							$d = array('id' => $key['studio'], 'name' => trim($td['name']));
							$datastudio[] = $d;
						}
					}
					else {
						$td = $colstudio->findone(array('_id' => new MongoId($key['studio'])));				
						$d = array('id' => $key['studio'], 'name' => trim($td['name']));
						$datastudio[] = $d;
					}
				}
			
			$var = array(
				'datamovie' => $datamovie,
				'datastudio' => $datastudio,
				'data' => $data
			);
			
			$this->render("","report/dash_event.php", $var);
		}
		
		public function event_detail(){
			$studio = isset($_POST['studio']) ? $_POST['studio'] : '';
			$bulan = isset($_POST['bulan']) ? $_POST['bulan'] : '';
			$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : '';
			$idfilm = isset($_POST['idfilm']) ? $_POST['idfilm'] : '';
			
			$db = Db::init();
			$colsch = $db->schedules;
			
			$bb = $bulan;
			$tt = $tahun;
			$start = strtotime($tt.'-'.$bb.'-01 00:00:00');
			$end = strtotime('+1 month', $start);
			
			$q = array(
				'movie' => $idfilm,
				'studio' => $studio,
				'scheduledate' => array('$gt' => $start, '$lt' => $end)
			);
			$carijadwal = $colsch->find($q)->sort(array('scheduledate' => 1));
			$countcarjadwal = $colsch->count($q);
			
			$var = array(
				'studio' => $studio,
				'carijadwal' => $carijadwal,
				'jumlahjadwal' => $countcarjadwal
			);

			$this->render("","report/detail_report_event.php", $var);
			
		}
		public function dash_contributor(){
			$idmovie = isset($_GET['idmovie']) ? $_GET['idmovie'] : '';
			
			$db = Db::init();
			//$page = $this->getPage();
			$colmovie = $db->movies;
			$trx = $db->transactions;
			$colstudio = $db->studios;
			$colsch = $db->schedules;
			
			$datamovie = $colmovie->find(array('contributor' => $_SESSION['contributor']));
			$data = array();
			if(strlen(trim($idmovie)) > 0)
				$data = $colmovie->findone(array('_id' => new MongoId($idmovie)));
			
			$dataschedule = $colsch->find(array('movie' => $idmovie))->sort(array('studio' => 1));
			
			$datastudio = array();
			foreach ($dataschedule as $key) {
				if (count($datastudio) > 0){
					$i = 0;
					$ii = 0;
					foreach ($datastudio as $ddst) {
						if($ddst['id'] == $key['studio'])
							$i++;
						$ii++;
					}
					if ($i == 0) {
						$td = $colstudio->findOne(array('_id' => new MongoId($key['studio'])));
						$d = array('id' => $key['studio'], 'name' => trim($td['name']));
						$datastudio[] = $d;
					}
				}
				else {
					$td = $colstudio->findone(array('_id' => new MongoId($key['studio'])));				
					$d = array('id' => $key['studio'], 'name' => trim($td['name']));
					$datastudio[] = $d;
				}
			}
			
						
			$var = array(
				'datamovie' => $datamovie,
				'data' => $data,
				'idm' => $idmovie,
				'datastudio' => $datastudio
			);

			$this->render("","report/dash_producer.php", $var);
		}
		
		public function contributor_detail(){
			$studio = isset($_POST['studio']) ? $_POST['studio'] : '';
			$bulan = isset($_POST['bulan']) ? $_POST['bulan'] : '';
			$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : '';
			$idfilm = isset($_POST['idfilm']) ? $_POST['idfilm'] : '';
			
			
			$db = Db::init();
			$colsch = $db->schedules;
			
			$bb = $bulan;
			$tt = $tahun;
			$start = strtotime($tt.'-'.$bb.'-01 00:00:00');
			$end = strtotime('+1 month', $start);
			
			$q = array(
				'movie' => $idfilm,
				'studio' => $studio,
				'scheduledate' => array('$gt' => $start, '$lt' => $end)
			);
			$carijadwal = $colsch->find($q)->sort(array('scheduledate' => 1));
			$countcarjadwal = $colsch->count($q);
			
			$var = array(
				'studio' => $studio,
				'carijadwal' => $carijadwal,
				'jumlahjadwal' => $countcarjadwal
			);

			$this->render("","report/detail_report_producer.php", $var);
		}

		public function dash_pengelola(){
			$idmovie = isset($_GET['idmovie']) ? $_GET['idmovie'] : '';	
			$db = Db::init();
			$colsch = $db->schedules;
			$colmovie = $db->movies;
			
			$data = array();
			if(strlen(trim($idmovie)) > 0)
				$data = $colmovie->findone(array('_id' => new MongoId($idmovie)));
			$carisch = $colsch->find(array('clientid' => trim($_SESSION['client'])));
			
			$datafilm = array();
			foreach ($carisch as $cs) {
				if (count($datafilm) > 0){
					$i = 0;
					$ii = 0;
					foreach ($datafilm as $ddf) {
						if($ddf['id'] == $cs['movie'])
							$i++;
						$ii++;
					}
					if ($i == 0) {
						$td = $colmovie->findOne(array('_id' => new MongoId($cs['movie'])));
						$d = array('id' => $cs['movie'], 'name' => trim($td['name']),);
						$datafilm[] = $d;
					}
				}
				else {
					$td = $colmovie->findone(array('_id' => new MongoId($cs['movie'])));				
					$d = array('id' => $cs['movie'], 'name' => trim($td['name']));
					$datafilm[] = $d;
				}
			}
			
			$var = array(
				'datafilm' => $datafilm,
				'data' => $data
			);
			
			$this->render("","report/dash_pengelola.php", $var);
		}

		public function pengelola_detail(){
			$bulan = isset($_POST['bulan']) ? $_POST['bulan'] : '';
			$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : '';
			$idfilm = isset($_POST['idfilm']) ? $_POST['idfilm'] : '';
			
			$db = Db::init();
			$colsch = $db->schedules;
			
			$bb = $bulan;
			$tt = $tahun;
			$start = strtotime($tt.'-'.$bb.'-01 00:00:00');
			$end = strtotime('+1 month', $start);
			
			$q = array(
				'movie' => $idfilm,
				'clientid' => $_SESSION['client'],
				'scheduledate' => array('$gt' => $start, '$lt' => $end)
			);
			
			$carijadwal = $colsch->find($q)->sort(array('scheduledate' => 1));
			$countcarjadwal = $colsch->count($q);
			
			$var = array(
				'idfilm' => $idfilm,
				'carijadwal' => $carijadwal,
				'jumlahjadwal' => $countcarjadwal
			);

			$this->render("","report/detail_report_pengelola.php", $var);
		}
	}
?>