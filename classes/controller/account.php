<?php
class account_controller extends controller
{
	public function register()
	{
		if(!empty($_POST))
		{
			if(isset($_POST['name']))
				$name = $_POST['name'];
			if(isset($_POST['email']))
				$email = $_POST['email'];
			if(isset($_POST['password']))
				$password = $_POST['password'];
			
			$db = Db::init();
			$userdb = $db->user_guests;
			
			$validator = new Validator();
			$parname = array(
				'table' => 'user_guests',
				'field' => 'email'
			);
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('email', array('require','unique' => $parname, 'minlength' => 3));
			$validator->addRule('password', array('require', 'minlength' => 3));
			$validator->setData(
                array(
               		'name' => $name,
               		'email' => $email,
               		'password' => $password,
              )
			);
			if($validator->isValid()) 
			{
				$h = new helper();
				$salt = helper::getRandomString();
				$newpassword = $h->createPassword($salt, $password);
				
				$data = array(
					'tipe' => 'unverified',
					'name' => $name,
					'email' => $email,
					'password' => $newpassword,
					'user_salt' => $salt,
					'time_created' => time()
				);
				$userdb->insert($data);
				echo "SUCCESS";
			}
			else
			{
				$error = $validator->getErrors();
				echo json_encode($error);
			}
		}
	}
	
	public function login()
	{
		if(!empty($_POST))
		{
			if(isset($_POST['email']))
				$email = $_POST['email'];
			if(isset($_POST['password']))
				$password = $_POST['password'];
			$db = Db::init();
			$userdb = $db->user_guests;
			
			$validator = new Validator();
			$paraccount = array(
				'table' => 'user_guests',
				'email' => $email
			);
			
			$validator->addRule('email', array('require'));
			$validator->addRule('password', array('require'));
			$validator->addRule('account', array('checkaccount' => $paraccount));
			$validator->setData(
                array(
               		'email' => $email,
               		'password' => $password
              )
			);
			
			if($validator->isValid())
			{
				$datauserone = $userdb->findOne(array("email" => $email));
				$d = array(
					'last_login' => time()
				);
				if(password_verify($password,$datauserone['password']))
				{
					$newdata = array('$set' => $d);
					$userdb->update(array("email" => $email), $newdata);
					$_SESSION['userid'] = $datauserone['_id'];
					$_SESSION['name'] = $datauserone['name'];
					$_SESSION['email'] = $datauserone['email'];
					echo "SUCCESS";
					exit;
				}
				else
				{
					$error = array("password" => array("Password Salah"));
					echo json_encode($error);
				} 
			}
			else
			{
				$error = $validator->getErrors();
				echo json_encode($error);
				exit;
			}
		}	
	}
	
	public function logout() {
		
		session_destroy ();
		$pagename = "/welcome/index";
		if(isset($_GET['pagename']))
		{
			$pagename = $_GET['pagename'];
		}
		header( 'Location: '.$pagename ) ;
		exit();
	}	
	
}

?>