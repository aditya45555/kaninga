<?php
class studiodetail_controller extends controller
{
	public function index()
	{
		$render = "studiodetail/index.php";
		$db = Db::init();
		$studio = $db->studios;
		$galeristudio = $db->galeristudios;
		
		$css[] = "/public/css/external/idangerous.swiper.css";
		$css[]=  "/public/css/leaflet.css";
		
		$js[] = "/public/js/external/idangerous.swiper.min.js";
		$js[] = "/public/js/leaflet.js";
		$js[] = "/public/js/maplocationstudiodetail.js";
		
		
		if(isset($_GET['studioid']))
		{
			$datenow = time();
			$studioid = $_GET['studioid'];
			$datastudio = $studio->findOne(array("_id" => new MongoId(trim($studioid))));
			$datagaleristudio = $galeristudio->find(array("studio" => trim($studioid)));
			$var = array(
				'datenow' => $datenow,
				'studioid' => $studioid,
				'datastudio' => $datastudio,
				'datagaleristudio' => $datagaleristudio,
				'css' => $css,
				'js' => $js
			);	
			$this->render("studiodetail", $render, $var);
		}
		else
		{
			$this->redirect("/studio/index");
		}
		
	}
	
	public function getlatlong()
	{
		if(isset($_GET['studioid']))
		{
			$db = Db::init();
			$studioid = $_GET['studioid'];
			$studiodb = $db->studios;
			
			$datastudio = $studiodb->findOne(array("_id" => new MongoId(trim($studioid))));
			
			$arr=array();
			if(isset($datastudio['latitude']))
			{
				$description="<h2>".$datastudio['name']."</h2>".
				"<p>".$datastudio['address']."</p>";	
				$p=array(
					"latitude"=>$datastudio['latitude'],
					"longitude"=>$datastudio['longtitude'],
					"description"=>$description
				);
			$arr[]=$p;	
			}		
			
			echo json_encode($arr);
		}
	}
}

?>