<?php
	class commingsoon1_controller extends controller{
		
		public function index(){
			$idmovi = isset($_GET['idmovi']) ? $_GET['idmovi'] : '';
			$idstudio = isset($_GET['studio']) ? $_GET['studio'] : '';
			$kota = isset($_GET['kota']) ? $_GET['kota'] : '';
			$movics = isset($_GET['movic']) ? $_GET['movic'] : '';
			
			$db = Db::init();
			$colmov = $db->movies;
			$colstd = $db->studios;
			$coltown = $db->place_towns;
			
			$datamov = $colmov->find(array('commingsoon' => 'yes'));
			$datatown = $coltown->find();
			$datastd = $colstd->find()->sort(array('name' => 1));
			
			$datakota = array();
			$search = array('Kota', 'Kabupaten');
			
			foreach ($datastd as $key) {
				if (count($datakota) > 0){
					$i = 0;
					$ii = 0;
					foreach ($datakota as $ddst) {
						if($ddst['id'] == $key['towns'])
							$i++;
						$ii++;
					}
					if ($i == 0) {
						$td = $coltown->findOne(array('_id' => new MongoId($key['towns'])));
						$tn = str_replace($search, '', $td['name']);
						$d = array('id' => $key['towns'], 'name' => trim($tn));
						$datakota[] = $d;
					}
				}
				else {
					$td = $coltown->findone(array('_id' => new MongoId($key['towns'])));				
					$tn = str_replace($search, '', $td['name']);
					$d = array('id' => $key['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			$data = array();
				if(strlen(trim($movics)) > 0)
				$data = $colmov->findone(array('_id' => new MongoId($movics)));
						
			$var = array(
				'datamoviecs' => $datamov,
				'datakota'  => $datakota,
				'idmovi' => $idmovi,
				'idstudio' => $idstudio,
				'idkota' => $kota,
				'idmovics' => $movics,
				'data' => $data
			);
			$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
			$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
			$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';
			$this->js[] = '/public/js/controller/commingsoon.js';
			
			$this->render('commingsoon', 'commingsoon/index1.php', $var);
		
		}
	}
?>