<?php
	class aboutus_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$col = $db->preferences;
			
			$dtpref = $col->find();
			
			$var = array(
				'data' => $dtpref
			);
			$this->render("aboutus", "aboutus/index.php", $var);
		}
	}
?>