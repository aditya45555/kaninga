<?php
class studio_controller extends controller
{
	public function index() {
		$idkota = isset($_GET['kota']) ? $_GET['kota'] : '';
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		
		$db = Db::init();
		$studio = $db->studios;
		$t = $db->place_towns;
		
		$dstudio = $studio->find();
		$dtt = $t->find()->sort(array('name' => 1));
		$datakota = array();
		$search = array('Kota', 'Kabupaten');
		foreach($dstudio as $dst) {
			if(count($datakota) > 0) {
				$n=0;
				$nn=0;
				foreach($datakota as $ddst) {
					if($ddst['id'] == $dst['towns'])
						$n++;
					$nn++;
				}
				if($n == 0) {
					$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
					$tn = str_replace($search, '', $tt['name']);
					$d = array('id' => $dst['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			else {
				$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
				$tn = str_replace($search, '', $tt['name']);
				$d = array('id' => $dst['towns'], 'name' => trim($tn));
				$datakota[] = $d;
			}
		}		
		usort($datakota, function ($a, $b) {
		    return strcmp($a['name'], $b['name']); 
		});
		
		$name = '';
		if(strlen(trim($idkota)) > 0) {
			$dtt = $t->findone(array('_id' => new MongoId($idkota)));
			$name = str_replace($search, '', $dtt['name']);
		}
		else {
			foreach($datakota as $dtk) {
				$name = str_replace($search, '', $dtk['name']);
				$idkota = trim($dtk['id']);
				break;
			}
		}		
		
		$data = array();
		if(strlen(trim($id)) > 0)
			$data = $studio->findone(array('_id' => new MongoId($id)));
		else {
			$col = $studio->find(array('towns' => $idkota))->sort(array('name' => 1))->limit(1);
			foreach($col as $k=>$v) {
				$data = $v;
			}
		}	
		
		$var = array(
			'idkota' => $idkota,
			'name' => $name,
			'datakota' => $datakota,
			'data' => $data
		);
		
		$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
		$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
		$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';
		
		$this->js[]="/public/js/leaflet.js";
		$this->css[]="/public/css/leaflet.css";
		$this->js[]="/public/js/maplocationread.js";
		
		$this->render('studio', 'studio/index2.php', $var);
	}
	
	public function getlatlong()
	{
		$db = Db::init();
		$studio = $db->studios;
		$id=$_GET['id'];
		if($id!=null)
		{
			$data = $studio->find(array('_id' => new MongoID($_GET['id'])));
		}
		else
		{
			$data = $studio->find();
		}
		
		$arr=array();
		foreach ($data as $dat ) 
		{
			if(isset($dat['latitude']))
			{
				$description="<h2>".$dat['name']."</h2>".
				"<p>".$dat['address']."</p>";	
				$p=array(
					"latitude"=>$dat['latitude'],
					"longitude"=>$dat['longtitude'],
					"description"=>$description,
					"id" => $id
			);
			$arr[]=$p;	
			}		
		}
		echo json_encode($arr);
	}
	
	/*public function index()
	{
		$render = "studio/index.php";
		$db = Db::init();
		$studio = $db->studios;
		$t = $db->place_towns;
		
		$dstudio = $studio->find();
		$dtt = $t->find()->sort(array('name' => 1));
		$datakota = array();
		$search = array('Kota', 'Kabupaten');
		foreach($dstudio as $dst) {
			if(count($datakota) > 0) {
				$n=0;
				$nn=0;
				foreach($datakota as $ddst) {
					if($ddst['id'] == $dst['towns'])
						$n++;
					$nn++;
				}
				if($n == 0) {
					$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
					$tn = str_replace($search, '', $tt['name']);
					$d = array('id' => $dst['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			else {
				$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
				$tn = str_replace($search, '', $tt['name']);
				$d = array('id' => $dst['towns'], 'name' => trim($tn));
				$datakota[] = $d;
			}
		}		
		usort($datakota, function ($a, $b) {
		    return strcmp($a['name'], $b['name']); 
		});
		
		$page = $this->getPage();
        $limit = 12;
        $skip = (int)($limit * ($page - 1));
		$countpagging = 0 ;
			
		if(isset($_GET['cityid']))
		{
			$cityid = $_GET['cityid']; 
			
			if(strtoupper($cityid)!="ALL" && strlen($cityid)>0)
			{
				$datastudio = $studio->find(array("towns"=>trim($cityid)))->sort(array('time_created' => -1))->limit($limit)->skip($skip);	
				$countpagging = $studio->count(array("towns" => trim ($cityid)));
			}
			else
			{
				$datastudio = $studio->find()->sort(array('time_created' => -1))->limit($limit)->skip($skip);
				$countpagging = $studio->count();
			}
		}
		else
		{
			$cityid = null; 	
			$datastudio = $studio->find()->sort(array('time_created' => -1))->limit($limit)->skip($skip);	
			$countpagging = $studio->count();
		}
	
		$_SESSION['cityid'] = $cityid;
		$datastudiodistinct = $studio->distinct("towns");
		$countdatastudiodistinct = count($datastudiodistinct);
		
		$pg = new Pagination();
		$pg_url = "/studio/index?cityid=".$cityid."&&page=";
		$pg->pag_url = $pg_url;
		$pg->calculate_pages($countpagging, $limit, $page);
		
		$js [] = "/public/js/getdatafilterstudio.js";
		$var = array(
			'js' => $js,
			'cityid' => $cityid,
			'datastudio' => $datastudio,
			'datastudiodistinct' => $datastudiodistinct,
			'countdatastudiodistinct' => $countdatastudiodistinct,
			'page' => $page,
			'limit' => $limit,
			'skip' => $skip+1,
			'pagination' => $pg->Show()
		);	
		$this->render("studio", $render, $var);
	}*/	
}
?>