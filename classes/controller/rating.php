<?php
class rating_controller extends controller
{
	public function insertrating()
	{
		if(!empty($_POST)&&isset($_SESSION['userid']))
		{
			$movieid = $_POST['movieid'];
			$point = $_POST['point'];
			
			$db = Db::init();
			$ratingmoviedb = $db->rating_movies;
			$countvalidasi = $ratingmoviedb->count(array("movie" => $movieid,"user" => trim($_SESSION['userid']))); 
			
			if($countvalidasi>0)
			{
				//updaterating
				$d = array(
					'point' => trim($point)
				);
				$newdata = array('$set' => $d);
				$ratingmoviedb->update(array("movie" => $movieid,"user" => trim($_SESSION['userid']) ), $newdata);
				echo "SUCCESS";
				exit;
			}
			else
			{
				//insert
				$p = array(
					'movie' => trim($movieid),
					'user' => trim($_SESSION['userid']),
					'point' => trim($point),
					"time_created" => time()
				);
				$ratingmoviedb->insert($p);
				echo "SUCCESS";
				exit;
			}
			
		}
		else
		{
			echo "NOT SUCCESS";
		}
	}
}
?>