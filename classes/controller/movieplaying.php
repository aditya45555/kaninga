<?php
class movieplaying_controller extends controller
{
	public function index() {
		$idkota = isset($_GET['kota']) ? $_GET['kota'] : '';
		$idstudio = isset($_GET['studio']) ? $_GET['studio'] : '';
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		
		$db = Db::init();
		$studio = $db->studios;
		$movie = $db->movies;
		$t = $db->place_towns;
		
		$dstudio = $studio->find();
		$dtt = $t->find()->sort(array('name' => 1));
		$datakota = array();
		$search = array('Kota', 'Kabupaten');
		foreach($dstudio as $dst) {
			if(count($datakota) > 0) {
				$n=0;
				$nn=0;
				foreach($datakota as $ddst) {
					if($ddst['id'] == $dst['towns'])
						$n++;
					$nn++;
				}
				if($n == 0) {
					$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
					$tn = str_replace($search, '', $tt['name']);
					$d = array('id' => $dst['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			else {
				$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
				$tn = str_replace($search, '', $tt['name']);
				$d = array('id' => $dst['towns'], 'name' => trim($tn));
				$datakota[] = $d;
			}
		}		
		usort($datakota, function ($a, $b) {
		    return strcmp($a['name'], $b['name']); 
		});
		
		$name = '';
		if(strlen(trim($idkota)) > 0) {
			$dtt = $t->findone(array('_id' => new MongoId($idkota)));
			$name = str_replace($search, '', $dtt['name']);
		}
		else {
			foreach($datakota as $dtk) {
				$name = str_replace($search, '', $dtk['name']);
				$idkota = trim($dtk['id']);
				break;
			}
		}
				
		$datastudio = $studio->find(array('towns' => $idkota))->sort(array('subdistrict' => 1));
		if(strlen(trim($idstudio)) == 0) {			
			foreach($datastudio as $k=>$v) {
				$idstudio = $k;
				break;
			}	
		}
		
		$datamovie = $movie->find();
		
		$data = array();
		if(strlen(trim($id)) > 0)
			$data = $movie->findone(array('_id' => new MongoId($id)));
		
		$var = array(
			'idkota' => $idkota,
			'idstudio' => $idstudio,
			'id' => $id,
			'name' => $name,
			'datakota' => $datakota,
			'datastudio' => $datastudio,
			'datamovie' => $datamovie,
			'data' => $data
		);
		
		$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
		$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
		$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';
		$this->js[] = '/public/js/controller/movieplaying.js';
		
		$this->render('nowplaying', 'movieplaying/index2.php', $var);
	}	
	
	/*public function index()
	{
		$db = Db::init();
		$studio = $db->studios;
		$movie = $db->movies;
		$genre = $db->genres;
		$commentmovie = $db->commentmovies;
		$scheduledb = $db->schedules;
		$t = $db->place_towns;
		
		$page = $this->getPage();
        $limit = 5;
        $skip = (int)($limit * ($page - 1));
		
		$dstudio = $studio->find();
		$dtt = $t->find()->sort(array('name' => 1));
		$datakota = array();
		$search = array('Kota', 'Kabupaten');
		foreach($dstudio as $dst) {
			if(count($datakota) > 0) {
				$n=0;
				$nn=0;
				foreach($datakota as $ddst) {
					if($ddst['id'] == $dst['towns'])
						$n++;
					$nn++;
				}
				if($n == 0) {
					$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
					$tn = str_replace($search, '', $tt['name']);
					$d = array('id' => $dst['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			else {
				$tt = $t->findone(array('_id' => new MongoId($dst['towns'])));				
				$tn = str_replace($search, '', $tt['name']);
				$d = array('id' => $dst['towns'], 'name' => trim($tn));
				$datakota[] = $d;
			}
		}		
		usort($datakota, function ($a, $b) {
		    return strcmp($a['name'], $b['name']); 
		});
		
		if(isset($_GET['date']))
		{
			if(strlen($_GET['date'])>0)
			{
				$date = date("Y-m-d",strtotime($_GET['date']));
			}
			else
			{
				$date = date("Y-m-d",time());
			}
			
			$studioid = $_GET['studioid'];
			
			if(strtoupper($studioid)!="ALL" && strlen($studioid)>0)
			{
				$datastudiofilter = $studio->find(array("_id" => New MongoId(trim($studioid))));	
			}
			else
			{
				$datastudiofilter = $studio->find();
			}
			
		}
		else
		{
			$date = date("Y-m-d",time());
			$datastudiofilter = $studio->find();
			$studioid = null;
		}
		
		$_SESSION['studioid'] = trim($studioid);
		$_SESSION['date'] = $date;
		
		$render = "movieplaying/index2.php";
		$datenow = strtotime($date);
		
		$datagenre = $genre->find()->limit(10);
		$datastudio = $studio->find();
		$datastudiodistinct = $studio->distinct("towns");
		$countdatastudiodistinct = count($datastudiodistinct);
		$datacomment = $commentmovie->find()->limit(10)->sort(array('time_created'=>-1));
		$datacommingsoon=$movie->find()->limit(10);
		
		if(strlen($studioid)>0 && strtoupper($studioid)!="ALL")
		{
			$countpagging = count($scheduledb->distinct("movie",array("active" => "1","scheduledate" => $datenow+1,"studio" => $studioid)));	
			$iddatamovie = $scheduledb->distinct("movie",array("active" => "1","scheduledate" => $datenow+1,"studio" => $studioid));
		}
		else
		{
			$countpagging = count($scheduledb->distinct("movie",array("active" => "1","scheduledate" => $datenow+1)));
			$iddatamovie = $scheduledb->distinct("movie",array("active" => "1","scheduledate" => $datenow+1));
		}
		
		
		$pg = new Pagination();
		$pg_url = "/movieplaying/index?studioid=".$_SESSION['studioid']."&&date=".$_SESSION['date']."&&page=";
        $pg->pag_url = $pg_url;
		$pg->calculate_pages($countpagging, $limit, $page);
		
		//$this->css[] = '/public/jcarousellite/style/style-demo.css';
		
		$this->js[]= "/public/js/controller/postrating.js";		
		$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
		$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
		$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';		
		$this->js[] = '/public/js/controller/movieplaying.js';
		$js[]="/public/js/getdatafilter.js";
		
		$var = array(
			'js' => $js,
			'datakota' => $datakota,
			'countpagging' => $countpagging,
			'iddatamovie'=> $iddatamovie,
			'datastudio' => $datastudio,
			'datastudiofilter' => $datastudiofilter,
			'studioid' => $studioid,
			'datastudiodistinct' => $datastudiodistinct,
			'datagenre' => $datagenre,
			'datacomment' => $datacomment,
			'datacommingsoon' => $datacommingsoon,
			'countdatastudiodistinct' => $countdatastudiodistinct,
			'datenow' => $datenow,
			'skip' => $skip,
			'limit' => $limit,
			'page' => $page,
			'pagination' => $pg->Show()
		);
		
		$this->render("movieplaying", $render, $var);
		
	}*/					
}