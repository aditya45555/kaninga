<?php
	class buyticket_controller extends controller{
		
		public function index(){
			$idmovi = isset($_GET['idmovi']) ? $_GET['idmovi'] : '';
			$idkota = isset($_GET['idkota']) ? $_GET['idkota'] : '';
			//$kotaku = isset($_POST['kotaku']) ? $_POST['kotaku'] : '';
			
		
			$db = Db::init();
			
			$colmovie = $db->movies;
			
			$datamovienw = $colmovie->find(array('commingsoon' => 'not'))->sort(array('time_created' => 1));
			$datamoviecs = $colmovie->find(array('commingsoon' => 'yes'))->sort(array('time_created' => 1));
			
			$data = array();
			if(strlen(trim($idmovi)) > 0)
			$data = $colmovie->findone(array('_id' => new MongoId($idmovi)));
			
			$colsch = $db->schedules;
			$coltwn = $db->place_towns;
			$colstd = $db->studios;
			
			$datatown = $coltwn->find();
			$datastd = $colstd->find()->sort(array('name' => 1));
			
			$datakota = array();
			$search = array('Kota', 'Kabupaten');
			
			foreach ($datastd as $key) {
				if (count($datakota) > 0){
					$i = 0;
					$ii = 0;
					foreach ($datakota as $ddst) {
						if($ddst['id'] == $key['towns'])
							$i++;
						$ii++;
					}
					if ($i == 0) {
						$td = $coltwn->findOne(array('_id' => new MongoId($key['towns'])));
						$tn = str_replace($search, '', $td['name']);
						$d = array('id' => $key['towns'], 'name' => trim($tn));
						$datakota[] = $d;
					}
				}
				else {
					$td = $coltwn->findone(array('_id' => new MongoId($key['towns'])));				
					$tn = str_replace($search, '', $td['name']);
					$d = array('id' => $key['towns'], 'name' => trim($tn));
					$datakota[] = $d;
				}
			}
			
			$var = array(
				'datamovienw' => $datamovienw,
				'datamoviecs' => $datamoviecs,
				'idmovi' => $idmovi,
				'data' => $data,
				'datakota' => $datakota,
				'idkota' => $idkota
			);
			$this->js[] = '/public/jcarousellite/script/jquery.easing-1.3.js';
			$this->js[] = '/public/jcarousellite/script/jquery.mousewheel-3.1.12.js';
			//$this->js[] = '/public/jcarousellite/script/jquery.jcarousellite.js';
			$this->js[] = '/public/js/cekkursi.js';
			
			$this->render("buyticket", "buyticket/index.php", $var);
		}

		
		public function schstudio(){
			$kotaku = isset($_GET['kotaku']) ? $_GET['kotaku'] : '';	
			$db = Db::init();
			$colstd = $db->studios;
			
			$dtstd = $colstd->find(array('towns' => $kotaku));
			$isi = "<option value='all'>ALL</option>";
			foreach ($dtstd as $key) {
				$isi .= "<option value='".$key['_id']."'>".$key['name']."</option>";
			}
			echo $isi;
		}
		
		public function cariwaktu(){
			$teater = isset($_GET['teater']) ? $_GET['teater'] : '';
			$tanggal = isset($_GET['tanggal']) ? $_GET['tanggal'] : '';
			$idfilm = isset($_GET['idfilm']) ? $_GET['idfilm'] : '';
			
			$db = Db::init();
			$colsch = $db->schedules;
			
			$caritgl = strtotime($tanggal.'00:00:00');
			$end = strtotime($tanggal.'23:59:59');
			
			$testawal = date('d-m-Y H:i:s',$caritgl);
			$testend = date('d-m-Y H:i:s',$end);
			
			$dtsch = $colsch->find(array(
				'movie' => $idfilm,
				'studio' => $teater,
				'scheduledate' => array('$gt' => $caritgl, '$lt' => $end)
			));
			
			$isi = '';
			foreach ($dtsch as $key) {
				$isi .= "<option value='".$key['scheduletime']."'>".$key['scheduletime']."</option>";
			}
			if ($isi == ""){
				echo '<option value="">Waktu tidak tersedia</option>';
			}else {
				echo $isi;
			}
		}
		
		public function caridata(){
			$kota = isset($_POST['kotav']) ? $_POST['kotav'] : '';
			$tanggal = isset($_POST['tanggalv']) ? $_POST['tanggalv'] : '';
			$jumlah = isset($_POST['jumlahv']) ? $_POST['jumlahv'] : '';
			$teater = isset($_POST['teaterv']) ? $_POST['teaterv'] : '';
			$waktu = isset($_POST['waktuv']) ? $_POST['waktuv'] : '';
			$film = isset($_POST['idfilmv']) ? $_POST['idfilmv'] : '';
			
			$db = Db::init();
			$colsch = $db->schedules;
			$colseats = $db->seats;
			$colstd = $db->studios;
			
			$caritgl = strtotime($tanggal.'00:00:00');
			$end = strtotime($tanggal.'23:59:59');
			//$end = strtotime('+1 day', $caritgl);
						
			$isi = array();
			
			if ($teater == "all"){
				$findstd = $colstd->find(array('towns' => $kota));
				foreach ($findstd as $datastd) {
					$findsch = $colsch->find(array(
						'studio' => trim($datastd['_id']),
						'movie' => $film,
						'scheduledate' => array('$gte' => $caritgl, '$lt' => $end),
						'active' => '1'
					));

					$waktutayang = "";
					$tiketada = 0;
					$isidata = array();
					foreach ($findsch as $datasch) {
						$tiketada = $colseats->count(array('scheduleid' => trim($datasch['_id']), 'status' => "available"));
						$waktutayang = $datasch['scheduletime'];
										
						if ($jumlah <= $tiketada){
							$bayar = $jumlah * $datastd['price'];
							$isidata = array(
								"idsch" => $datasch['_id'],
								"name" => $datastd['name'],
								"tanggal" => $tanggal,
								"waktu" => $waktutayang,
								"harga" => $datastd['price'],
								"jumlah" => $jumlah,
								"statusbayar" => $bayar,
								"ket" => "ORDER NOW"
							);	
						}else {
							$isidata = array(
								"idsch" => $datasch['_id'],
								"name" => $datastd['name'],
								"tanggal" => $tanggal,
								"waktu" => $waktutayang,
								"harga" => $datastd['price'],
								"jumlah" => $jumlah,
								"statusbayar" => "SOLD OUT",
								"ket" => "NOT AVAILABLE"
							);
						}
					$isi[] = $isidata;	
					}
				} echo json_encode($isi);
			}else {
				$datastd = $colstd->findOne(array('_id' => new MongoId(trim($teater))));
					$findsch = $colsch->find(array(
						'studio' => $teater,
						'movie' => $film,
						'scheduledate' => array('$gte' => $caritgl, '$lt' => $end),
						'scheduletime' => $waktu,
						'active' => '1'
					));
					$tiketada = 0;
					$isidata = array();
					foreach ($findsch as $datasch) {
						$tiketada = $colseats->count(array('scheduleid' => trim($datasch['_id']), 'status' => "available"));
					
						if ($jumlah <= $tiketada){
							$bayar = $jumlah * $datastd['price'];
							$isidata = array(
								"idsch" => $datasch['_id'],
								"name" => $datastd['name'],
								"tanggal" => $tanggal,
								"waktu" => $waktu,
								"harga" => $datastd['price'],
								"jumlah" => $jumlah,
								"statusbayar" => $bayar,
								"ket" => "ORDER NOW"
							);	
						}else {
							$isidata = array(
								"idsch" => $datasch['_id'],
								"name" => $datastd['name'],
								"tanggal" => $tanggal,
								"waktu" => $waktu,
								"harga" => $datastd['price'],
								"jumlah" => $jumlah,
								"statusbayar" => "SOLD OUT",
								"ket" => "NOT AVAILABLE"
							);
						}
					$isi[] = $isidata;
					}
					echo json_encode($isi);
			}
		}
		
	}
?>