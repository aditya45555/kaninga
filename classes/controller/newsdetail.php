<?php
class newsdetail_controller extends controller
{
	public function index()
	{
		if(isset($_GET['newsid']))
		{
			$newsid = $_GET['newsid']; 
			$db=Db::init();
			$newsdb = $db->news;
			$page = $this->getPage();
	        $limit = 5;
	        $skip = (int)($limit * ($page - 1));
			$countpagging = 0 ;
			$datanews = $newsdb->findOne(array("_id"=> new MongoId(trim($newsid))));
			$var=array(
				"datanews" => $datanews,
			);
			$this->render("newsdetail", "newsdetail/index.php", $var);
		}
		else
		{
			$this->redirect("/notfound/index");
		}
	}
}
?>